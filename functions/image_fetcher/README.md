<!-- 
  This file is part of Presents.
  Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
  you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
  Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 
  SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
  SPDX-License-Identifier: GPL-3.0-or-later
-->

# 🤖 image_fetcher Documentation

Cloud function that recieves the URL of a product page as an input, and returns a json object containing the URL of the image preview for the product and also the image itself as a base64-encoded string of bytes

_Example input:_

"https://store.appwrite.io/collections/men/products/appwrite-mens-hoodie"

_Example output:_

```json
{
  "passed_url": "https://store.appwrite.io/collections/men/products/appwrite-mens-hoodie",
  "image_url": "http://cdn.shopify.com/s/files/1/0532/9397/3658/products/unisex-eco-hoodie-black-front-2-608bbdf530403_1024x.jpg?v=1619770876",
  "encoded_image": <STRING_OF_BASE64-ENCODED_IMAGE_BYTES>
}
```

### 📝 Environment Variables

List of environment variables used by this cloud function:

- **APPWRITE_FUNCTION_ENDPOINT** - Endpoint of Appwrite project
- **APPWRITE_FUNCTION_API_KEY** - Appwrite API Key