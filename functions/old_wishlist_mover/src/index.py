#  This file is part of Presents.
#  Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
#  you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
#  Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
#
#  SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
#  SPDX-License-Identifier: GPL-3.0-or-later

from appwrite.client import Client
from appwrite.exception import AppwriteException
from appwrite.permission import Permission
from appwrite.role import Role
from appwrite.query import Query
from appwrite.services.databases import Databases
import json

"""
  'req' variable has:
    'headers' - object with request headers
    'payload' - request body data as a string
    'variables' - object with function variables

  'res' variable has:
    'send(text, status)' - function to return text response. Status code defaults to 200
    'json(obj, status)' - function to return JSON response. Status code defaults to 200

  If an error is thrown, a response with code 500 will be returned.
"""


def create_wishlist_coll(user_id: str, database):
    db_id = 'wishlists_db'

    # Create the user's wishlist collection
    database.create_collection(
        database_id=db_id,
        collection_id=user_id,
        name=f"{user_id}'s wishlist",
        permissions=[
            Permission.read(Role.users()),
            Permission.update(Role.users()),
            Permission.create(Role.user(user_id)),
            Permission.delete(Role.user(user_id)),
        ]
    )
    # Add the 'Title' attribute to the new collection
    database.create_string_attribute(
        database_id=db_id,
        collection_id=user_id,
        key='title',
        size=255,
        required=True,
    )
    # Add the 'Price' attribute to the new collection
    database.create_float_attribute(
        database_id=db_id,
        collection_id=user_id,
        key='price',
        required=True
    )
    # Add the 'Link' attribute to the new collection
    database.create_url_attribute(
        database_id=db_id,
        collection_id=user_id,
        key='link',
        required=False
    )
    # Add the 'Priority' attribute to the new collection
    database.create_integer_attribute(
        database_id=db_id,
        collection_id=user_id,
        key='priority',
        required=True,
        min=1,
        max=10,
    )
    # Add the 'Donor UID' attribute to the new collection
    database.create_string_attribute(
        database_id=db_id,
        collection_id=user_id,
        key='donorUid',
        size=25,
        required=False,
    )


def main(req, res):
    client = Client()
    database = Databases(client)

    if not req.variables.get('APPWRITE_FUNCTION_ENDPOINT') or not req.variables.get('APPWRITE_FUNCTION_API_KEY'):
        print('Environment variables are not set. Function cannot use Appwrite SDK.')
    else:
        (
            client
            .set_endpoint(req.variables.get('APPWRITE_FUNCTION_ENDPOINT', None))
            .set_project(req.variables.get('APPWRITE_FUNCTION_PROJECT_ID', None))
            .set_key(req.variables.get('APPWRITE_FUNCTION_API_KEY', None))
            .set_self_signed(True)
        )

    userData_docs: list[dict] = database.list_documents(
        database_id='default',
        collection_id='userData',
        queries=[
            Query.limit(100)
        ]
    )['documents']

    print(f"{len(userData_docs)} users found:")

    for doc in userData_docs:
        print(f"Migrating {doc['username']}")

        # Get the user's uid and presents
        uid = doc['$id']
        try:
            presents = list(map(lambda p: json.loads(p), doc['presents']))
        except TypeError:
            # Create an empty list if the 'presents' field of the document was None
            presents = []

        # Check if a wishlist collection for the user exists
        try:
            database.get_collection(
                database_id='wishlists_db',
                collection_id=uid,
            )
        except AppwriteException:
            # And create it if it doesn't
            create_wishlist_coll(uid, database)

        for present in presents:
            try:
                database.create_document(
                    database_id='wishlists_db',
                    collection_id=uid,
                    document_id=present['id'],
                    data={
                        'title': present['title'],
                        'price': present['price'],
                        'link': present['link'],
                        'priority': present['priority'],
                        'donorUid': present['donorUid'],
                    }
                )
            except AppwriteException:
                # If the document already exists do nothing
                pass

        print('MIGRATED!')

    return res.send(f"{len(userData_docs)} wishlists has been moved")
