<!-- 
  This file is part of Presents.
  Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
  you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
  Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 
  SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
  SPDX-License-Identifier: GPL-3.0-or-later
-->

# 🤖 old_wishlist_mover Documentation

A cloud function do move old style wishlists (saved in the userData doc) to new style ones (collections in the wishlists_db with a doc for each wish)

_Example input:_

This function expects no input

_Example output:_

```
<N> wishlists have been moved
```

### 📝 Environment Variables

List of environment variables used by this cloud function:

- **APPWRITE_FUNCTION_ENDPOINT** - Endpoint of Appwrite project
- **APPWRITE_FUNCTION_API_KEY** - Appwrite API Key