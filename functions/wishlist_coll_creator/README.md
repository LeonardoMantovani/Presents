<!-- 
  This file is part of Presents.
  Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
  you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
  Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 
  SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
  SPDX-License-Identifier: GPL-3.0-or-later
-->

# 🤖 wishlist_coll_creator Documentation

A cloud function to create a new wishlist collection for users that still don't have one

_Example input:_

```
<USER_UID>
```

<!-- If input is expected, add example -->

_Example output:_

<!-- Update with your expected output -->

```json
{
  "collection": "CREATED",
  "title_attr": "CREATED",
  "price_attr": "CREATED",
  "link_attr": "CREATED",
  "priority_attr": "CREATED",
  "donorUid_attr": "CREATED"
}
```

### 📝 Environment Variables

List of environment variables used by this cloud function:

- **APPWRITE_FUNCTION_ENDPOINT** - Endpoint of Appwrite project
- **APPWRITE_FUNCTION_API_KEY** - Appwrite API Key