#  This file is part of Presents.
#  Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
#  you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
#  Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
#
#  SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
#  SPDX-License-Identifier: GPL-3.0-or-later

from appwrite.client import Client
from appwrite.permission import Permission
from appwrite.role import Role

# You can remove imports of services you don't use
from appwrite.services.databases import Databases

"""
  'req' variable has:
    'headers' - object with request headers
    'payload' - request body data as a string
    'variables' - object with function variables

  'res' variable has:
    'send(text, status)' - function to return text response. Status code defaults to 200
    'json(obj, status)' - function to return JSON response. Status code defaults to 200

  If an error is thrown, a response with code 500 will be returned.
"""


def main(req, res):
    client = Client()
    database = Databases(client)

    if not req.variables.get('APPWRITE_FUNCTION_ENDPOINT') or not req.variables.get('APPWRITE_FUNCTION_API_KEY'):
        print('Environment variables are not set. Function cannot use Appwrite SDK.')
    else:
        (
            client
            .set_endpoint(req.variables.get('APPWRITE_FUNCTION_ENDPOINT', None))
            .set_project(req.variables.get('APPWRITE_FUNCTION_PROJECT_ID', None))
            .set_key(req.variables.get('APPWRITE_FUNCTION_API_KEY', None))
            .set_self_signed(True)
        )

    payload = req.payload

    db_id = 'wishlists_db'
    coll_id = payload

    # Create the user's wishlist collection
    result0 = database.create_collection(
        database_id=db_id,
        collection_id=coll_id,
        name=f"{payload}'s wishlist",
        permissions=[
            Permission.read(Role.users()),
            Permission.update(Role.users()),
            Permission.create(Role.user(payload)),
            Permission.delete(Role.user(payload)),
        ]
    )
    # Add the 'Title' attribute to the new collection
    result1 = database.create_string_attribute(
        database_id=db_id,
        collection_id=coll_id,
        key='title',
        size=255,
        required=True,
    )
    # Add the 'Price' attribute to the new collection
    result2 = database.create_float_attribute(
        database_id=db_id,
        collection_id=coll_id,
        key='price',
        required=True
    )
    # Add the 'Link' attribute to the new collection
    result3 = database.create_url_attribute(
        database_id=db_id,
        collection_id=coll_id,
        key='link',
        required=False
    )
    # Add the 'Priority' attribute to the new collection
    result4 = database.create_integer_attribute(
        database_id=db_id,
        collection_id=coll_id,
        key='priority',
        required=True,
        min=1,
        max=10,
    )
    # Add the 'Donor UID' attribute to the new collection
    result5 = database.create_string_attribute(
        database_id=db_id,
        collection_id=coll_id,
        key='donorUid',
        size=25,
        required=False,
    )

    return res.json({
      "collection": "NOT CREATED" if result0 is None else "CREATED",
      "title_attr": "NOT CREATED" if result1 is None else "CREATED",
      "price_attr": "NOT CREATED" if result2 is None else "CREATED",
      "link_attr": "NOT CREATED" if result3 is None else "CREATED",
      "priority_attr": "NOT CREATED" if result4 is None else "CREATED",
      "donorUid_attr": "NOT CREATED" if result5 is None else "CREATED",
    })
