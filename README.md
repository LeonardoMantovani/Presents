<!--
  This file is part of Presents.
  Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
  you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
  Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 
  SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
  SPDX-License-Identifier: GPL-3.0-or-later
-->

# Presents: A Desired Surprise

![Presents Logo Landscape](assets/images/landscape.png)

---
### ReadMe Navigation:
1. [What is Presents](#what-is-presents)
2. [Features](#features)
3. [Roadmap](#roadmap)
    * [Why does this repo starts from Version 2?](#why-does-this-repo-starts-from-version-2)
4. [Technology Stack](#technology-stack)
5. [Contributing Guidelines](#contributing-guidelines)
    * [Setting up an Appwrite Server for Presents](#setting-up-an-appwrite-server-for-presents)
6. [Copyright and License](#copyright-and-license)
---

## What is Presents
Presents is the ultimate social-wishlist app that you didn't know you needed.
Why? Because with that one single app you will solve 3 huge problems:
1. Keeping losing the papers where you had written down the super cool product you saw and definitely wanted
2. Not knowing what to gift to a friend/relative but preferring to avoid the pathetic question: “hey what do you want as a gift?”
3. Having to think many different answers for the pathetic question above (one for each relative/friend who asks you before Christmas/your birthday/…)

So what are you waiting for? [Download the app](https://lezsoft.com/presents), share it with your friends and say goodbye to the Birthday-Gift-Panic!


## Features:
* Wishlist synchronized across all your devices where, for each Wish, you can add:
    * A title (required, for example *New Hoodie*)
    * An estimated price (required)
    * A link to a product page (which will be used to fetch a preview image too)
    * A "priority level" (the wishes with a higher priority will be displayed on top of the wishlist)
* Profile page for all users where you can see user's:
    * Full name
    * Username
    * Profile Picture
    * Birth Date
    * An *About* section white some information about his/her personality
* Wishlists of every user where you can see their wishes and reserve them to be the only one with that gift (don't worry, the owner of the wishlist won't be able to see which wishes have been reserved)

![Some Presents screenshots](assets/images/screenshots.png)

---

## Roadmap
- [x] V2 Android application:
    - [x] New unique look
    - [x] Wishes' URL image preview
    - [x] New FOSS Backend (to replace Firebase)
- [x] Dark Theme
- [x] Responsive Design and Web Application
- [X] Deep links to share users profiles
- [X] Database Migration from Firebase (version 1) to Appwrite
- [ ] Birthday reminder notification for favorite users
- [ ] Optional “upload image” field for wishes


### Why does this repo starts from version 2?
*Presents* v1 has been published on the *Google Play Store* on 14th October 2020, and it was using *Google Firebase* as backend.
After about a year from his first release, we decided to re-build it without using any proprietary software in order to protect our users data without having to share them with any third-party company.
So we started working on this *version 2* (that we decided to make *open-source*) replacing *Firebase* with *AppWrite*.

---

## Technology Stack
This new version of *Presents* has been completely rewritten, and it's using a `FLAW` Stack:
* The [**FL**utter framework](https://flutter.dev/) for a native, cross-platform frontend
* The [**A**pp**W**rite project](https://appwrite.io/) as an easy scalable and deployable while complete server backend

---

## Contributing Guidelines
We appreciate all contributions and *Pull Requests* since we think that cooperation between developers and users is a core part of open-source.
However, to ensure that the new code works flawlessly with the old one and share its "style", all *Pull Requests* will be reviewed by a LezSoft developer before being merged.

If you have any non-code suggestion please open an *Issue* here on Codeberg so that it will be discussed together with all the *Presents* community.

If you have any question feel free to reach us [by email](mailto:info@lezsoft.com)


### Setting up an AppWrite Server for Presents
To test *Presents*, we suggest to set up a local AppWrite server using a Docker container as explained in [Appwrite's Official Documentation](https://appwrite.io/docs/installation).
Then, from the AppWrite Web Console, you'll need to create a new project with `presents` as ID and, from the Project Settings, write down its `API Endpoint`.

#### Database setup
* Create a new database with ID `default` 
* Inside it, a collection with ID=`userData` and the following attributes:
  * `email` (email, required)
  * `username` (string, required)
  * `name` (string)
  * `birthday` (datetime)
  * `favUsers` (string array)
  * `presents` (string array)
  * `about` (string)
  * `profilePicId` (string)

#### Storage setup
* Create a new storage bucket with ID `profilePics`

#### Functions
* Create a new function with id `image_fetcher` both using the web GUI **or** the Appwrite CLI. The latter method is recommended and consists in few easy steps:
  * Install the AppWrite CLI following [the official documentation](https://appwrite.io/docs/command-line#installation)
  * Configure it with your AppWrite server endpoint using the command 
  `appwrite client --endpoint <YOUR_API_ENDPOINT>`
  * Log in to your AppWrite server using the command 
  `appwrite login`
  * Deploy the function (which is already present in this repository) using the command 
  `appwrite deploy function` and selecting `image_fetcher`

> NOTE: Different values can be used for the Appwrite IDs, but then they must be changed in `utils/constants.dart` and `appwrite.json` too
---

## Copyright and License
This project has been developed by [LezSoft](https://lezsoft.com).
All the rights on the *Presents* brand (with its name and logo) are reserved.

Presents is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Presents is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the [GNU General Public License](LICENSES/GPL-3.0-or-later.txt) for more details.