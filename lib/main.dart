/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_web_plugins/url_strategy.dart';
import 'package:go_router/go_router.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/theme.dart';
import 'package:presents/utils/global_shared_link.dart';
import 'package:presents/utils/presents_router.dart';
import 'package:provider/provider.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';

void main() {
  usePathUrlStrategy();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late StreamSubscription _intentDataStreamSubscription;

  @override
  void initState() {
    super.initState();

    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    _intentDataStreamSubscription = ReceiveSharingIntent.getTextStream().listen((String value) {
      // check if the link is a deep link (and in that case ignore it)
      if (value.contains('presents.lezsoft.com')) {
        return;
      }

      // get only the link (some apps share the link preceded by a message)
      int httpPos = value.indexOf('http');
      if (httpPos > 0) {
        value = value.substring(httpPos);
        globalSharedLink = value;
      } else if (httpPos == 0) {
        globalSharedLink = value;
      }
    }, onError: (err) {
      debugPrint("getLinkStream error: $err");
    });

    // For sharing or opening urls/text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then((String? value) {
      // check if the link is a deep link (and in that case ignore it)
      if (value?.contains('presents.lezsoft.com') ?? false) {
        return;
      }

      if (value != null) {
        int httpPos = value.indexOf('http');
        if (httpPos > 0) {
          value = value.substring(httpPos);
        } else if (httpPos == -1) {
          value =
              null; //if .indexOf('http') returns -1 means the text is not a link
        }
      }
      globalSharedLink = value;
    });
  }

  @override
  void dispose() {
    _intentDataStreamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeManager>(
          create: (context) => ThemeManager(),
        ),
        ChangeNotifierProvider<AuthService>(
          create: (context) => AuthService(),
        ),
      ],
      child: Builder(
        builder: (context) {
          final ThemeManager tM = Provider.of<ThemeManager>(context);
          final AuthService auth =
              Provider.of<AuthService>(context, listen: false);

          final GoRouter router = PresentsRouter(authService: auth).router;

          return MaterialApp.router(
            title: 'Presents',
            theme: ThemeManager.getThemeData(),
            darkTheme: ThemeManager.getThemeData(darkTheme: true),
            themeMode: tM.currThemeMode,
            routeInformationParser: router.routeInformationParser,
            routeInformationProvider: router.routeInformationProvider,
            routerDelegate: router.routerDelegate,
          );
        },
      ),
    );
  }
}
