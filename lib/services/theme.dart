/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Main colors used by Presents
class ColorPalette {
  static Color yellow = Colors.orangeAccent;
  static Color darkGrey = Colors.grey[850]!;
  static Color lightGrey = Colors.grey[700]!;
  static Color white = Colors.white;
  static Color error = Colors.pinkAccent;
}

class ThemeManager extends ChangeNotifier {
  // Constructor
  ThemeManager() {
    _getDefaultTheme();
  }

  late final SharedPreferences shPrefs;
  ThemeMode? currThemeMode;

  /// Asynchronously fills `prefs` and `currTheme` variables
  /// (loads default value for `currTheme` from shared_preferences)
  Future<void> _getDefaultTheme() async {
    shPrefs = await SharedPreferences.getInstance();
    bool? isDarkPreferred = shPrefs.getBool('darkThemePreferred');

    if (isDarkPreferred != null) {
      currThemeMode = isDarkPreferred ? ThemeMode.dark : ThemeMode.light;
    }
    else {
      currThemeMode = ThemeMode.system;
    }
    notifyListeners();
  }

  /// Returns the proper ThemeData object, based on the argument passed
  static ThemeData getThemeData({bool darkTheme = false}) {
    return ThemeData(
      fontFamily: 'Poppins',

      //region Color Scheme
      colorScheme: darkTheme ? ColorScheme.dark(
        background: ColorPalette.darkGrey,
        onBackground: ColorPalette.white,
        error: ColorPalette.error,
        onError: ColorPalette.white,
        primary: ColorPalette.yellow,
        onPrimary: ColorPalette.darkGrey,
        tertiary: ColorPalette.lightGrey,
        onTertiary: ColorPalette.white,
      ) : ColorScheme.light(
        background: ColorPalette.white,
        onBackground: ColorPalette.darkGrey,
        error: ColorPalette.error,
        onError: ColorPalette.white,
        primary: ColorPalette.yellow,
        onPrimary: ColorPalette.darkGrey,
        tertiary: ColorPalette.white,
        onTertiary: ColorPalette.darkGrey,
      ),
      //endregion

      //region Elevated Button Theme
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0)
          ),
          minimumSize: const Size(100.0, 50.0),
          textStyle: const TextStyle(
            fontFamily: 'Poppins',
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      //endregion

      //region Text Button Theme
      textButtonTheme: TextButtonThemeData(
        style: TextButton.styleFrom(
          foregroundColor: ColorPalette.darkGrey,
        ),
      ),
      //endregion

      //region Icon Theme
      iconTheme: IconThemeData(
        color: ColorPalette.darkGrey,
      ),
      //endregion

      sliderTheme: SliderThemeData(
        inactiveTickMarkColor: ColorPalette.darkGrey,
        inactiveTrackColor: ColorPalette.yellow,
        activeTickMarkColor: darkTheme ? ColorPalette.darkGrey : ColorPalette.yellow,
        activeTrackColor: darkTheme ? ColorPalette.white : ColorPalette.darkGrey,
        valueIndicatorColor: darkTheme ? ColorPalette.white : ColorPalette.darkGrey,
        valueIndicatorTextStyle: TextStyle(
          color: darkTheme ? ColorPalette.darkGrey : ColorPalette.white,
          fontFamily: 'Poppins',
        ),
        thumbColor: darkTheme ? ColorPalette.white : ColorPalette.darkGrey,
      ),

      //region Input Decoration Theme
      inputDecorationTheme: InputDecorationTheme(
        prefixIconColor: darkTheme ? ColorPalette.white : ColorPalette.darkGrey,
        floatingLabelStyle: TextStyle(
          color: darkTheme ? ColorPalette.white : ColorPalette.darkGrey,
          fontFamily: 'Poppins',
        ),
        fillColor: darkTheme ? ColorPalette.lightGrey : ColorPalette.white,
        filled: true,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
          borderSide: BorderSide(color: Colors.grey[darkTheme ? 900 : 100]!, width: 0.1),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
          borderSide: BorderSide(color: darkTheme ? ColorPalette.white : ColorPalette.darkGrey, width: 2),
        ),
      ),
      //endregion

      //region AppBar Theme
      appBarTheme: AppBarTheme(
        titleTextStyle: TextStyle(
          fontFamily: 'Poppins',
          fontSize: 20.0,
          color: ColorPalette.darkGrey
        ),
        iconTheme: IconThemeData(color: ColorPalette.darkGrey),
      ),
      //endregion

      //region Dialog Theme
      dialogTheme: DialogTheme(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
          side: BorderSide(color: darkTheme ? ColorPalette.lightGrey : ColorPalette.darkGrey, width: 1),
        )
      )
      //endregion

    );
  }

  /// Update current theme (and save the preference if `persistent` flag is true)
  void updateCurrTheme({required ThemeMode newThemeMode, bool persistent = true, bool silent = true}) {
    if (persistent) {
      switch (newThemeMode) {
        case ThemeMode.system:
          if (shPrefs.containsKey('darkThemePreferred')) {
            shPrefs.remove('darkThemePreferred');
          }
          break;
        case ThemeMode.light:
          shPrefs.setBool('darkThemePreferred', false);
          break;
        case ThemeMode.dark:
          shPrefs.setBool('darkThemePreferred', true);
          break;
      }
    }
    currThemeMode = newThemeMode;
    if (!silent) {
      notifyListeners();
    }
  }

}