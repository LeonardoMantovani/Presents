/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'dart:typed_data';

import 'package:appwrite/appwrite.dart';
import 'package:appwrite/models.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/models/present.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Class that handles all Database and Storage related tasks
class DatabaseService extends ChangeNotifier {
  // Constructor
  DatabaseService({required AuthService authService, required uid}) {
    _uid = uid;
    _auth = authService;
    _db = Databases(authService.client);
    _storage = Storage(authService.client);
    _functions = Functions(authService.client);
  }

  late final AuthService _auth;
  late final String _uid;
  late final Databases _db;
  late final Storage _storage;
  late final Functions _functions;

  //region USERDATA-RELATED STUFF

  /// Return a MyUser for the given document
  /// (can return it with empty favUser list if specified, to avoid infinite loop)
  Future<MyUser> _getMyUserFromDoc({
    required Document doc,
    bool withoutFavs = false
  }) async {
    List<MyUser> favUsers = List<MyUser>.empty(growable: true);
    // if favourite users are required and document's favUsers list isn't empty...
    if (!withoutFavs && doc.data['favUsers'] != "") {
      // get every favourite user and add it to the new list
      for (String userId in doc.data['favUsers']) {
        MyUser? usr = await getUserData(userId: userId);
        if (usr != null) {
          favUsers.add(usr);
        }
      }
    }

    // Get user's profile pic
    Image? profilePic;
    if (doc.data['profilePicId'] != null) {
      Uint8List picBytes = await _storage.getFilePreview(
          bucketId: Constants.profilePicsBucketID,
          fileId: doc.data['profilePicId']);

      profilePic = Image.memory(
        picBytes,
        fit: BoxFit.cover,
      );
    }

    return MyUser(
      uid: doc.$id,
      email: doc.data['email'],
      username: doc.data['username'],
      name: doc.data['name'],
      birthday: DateTime.tryParse(doc.data['birthday'] ?? ''),
      about: doc.data['about'],
      favUsers: favUsers,
      profilePicId: doc.data['profilePicId'],
      profilePic: profilePic,
      currency: doc.data['currency'] ?? '€',
    );
  }

  /// Return the MyUser matching the given user ID
  /// (or null if it doesn't exist)
  Future<MyUser?> getUserData({String? userId}) async {
    // check if it's asking data for another user
    bool isOtherUser = true;
    if (userId == null) {
      isOtherUser = false;
      userId = _uid;
    }

    Document userDoc = await _db.getDocument(
      databaseId: Constants.defaultDbID,
      collectionId: Constants.usersDataCollID,
      documentId: userId
    );

    return _getMyUserFromDoc(doc: userDoc, withoutFavs: isOtherUser);
  }

  /// Create a userData document in the database and return the just-created MyUser
  Future<MyUser> createUserData(
      {required String uid,
      required String email,
      required String username}) async {
    Document userDoc = await _db.createDocument(
        databaseId: Constants.defaultDbID,
        collectionId: Constants.usersDataCollID,
        documentId: uid,
        data: {
          'email': email,
          'username': username,
        });

    return _getMyUserFromDoc(doc: userDoc);
  }

  /// Returns a list of all already used usernames
  Future<List<String>> getAllUsernames() async {
    List<String> usernames = List<String>.empty(growable: true);

    List<Document> usersDataDocs = (await _db.listDocuments(
            databaseId: Constants.defaultDbID,
            collectionId: Constants.usersDataCollID))
        .documents;
    for (Document doc in usersDataDocs) {
      usernames.add(doc.data['username']);
    }

    return usernames;
  }

  /// Return a list of all existing users that contains the given string in their usernames
  Future<List<MyUser>> searchUser(String searchText, {bool byUsername=true}) async {
    List<MyUser> foundUsers = List<MyUser>.empty(growable: true);

    List<Document> usersDataDoc = (await _db.listDocuments(
      databaseId: Constants.defaultDbID,
      collectionId: Constants.usersDataCollID,
      queries: [
        Query.limit(50),  // This extends the limit of search results to 50 (default is 25)
        if (byUsername)
          Query.search('username', searchText),
        if (!byUsername)
          Query.search('name', searchText),
      ]
    )).documents;

    for (Document doc in usersDataDoc) {
      MyUser usr = await _getMyUserFromDoc(doc: doc, withoutFavs: true);
      foundUsers.add(usr);
    }

    return foundUsers;
  }

  /// Add the given user to the favourites of the current one
  /// (and notify listeners both of DatabaseService and AuthService)
  Future<void> addFavUser(MyUser user) async {
    List favIds = [];

    Document currUserDoc = await _db.getDocument(
        databaseId: Constants.defaultDbID,
        collectionId: Constants.usersDataCollID,
        documentId: _uid);

    favIds = currUserDoc.data['favUsers'];
    if (!favIds.contains(user.uid)) {
      favIds.add(user.uid);

      _db.updateDocument(
          databaseId: Constants.defaultDbID,
          collectionId: Constants.usersDataCollID,
          documentId: _uid,
          data: {
            'favUsers': favIds,
          });

      await _auth.getCurrUser();
      notifyListeners();
    }
  }

  /// Remove the given user from the favourites of the current one
  /// (and notify listeners both of DatabaseService and AuthService)
  Future<void> removeFavUser(MyUser user) async {
    List favIds = [];
    Document currUserDoc = await _db.getDocument(
      databaseId: Constants.defaultDbID,
      collectionId: Constants.usersDataCollID,
      documentId: _uid,
    );

    favIds = currUserDoc.data['favUsers'];

    if (favIds.contains(user.uid)) {
      favIds.remove(user.uid);

      _db.updateDocument(
          databaseId: Constants.defaultDbID,
          collectionId: Constants.usersDataCollID,
          documentId: _uid,
          data: {
            'favUsers': favIds,
          });

      await _auth.getCurrUser();
      notifyListeners();
    }
  }

  /// Update UserData document with the given new data
  Future updateUser({
    String? name,
    String? username,
    String? about,
    String? profilePicId,
    String? birthday,
    String? currency,
  }) async {
    String? newProfilePicId;
    if (profilePicId == 'null') {
      newProfilePicId = null;
    } else {
      newProfilePicId = profilePicId;
    }

    Document res = await _db.updateDocument(
        databaseId: Constants.defaultDbID,
        collectionId: Constants.usersDataCollID,
        documentId: _uid,
        data: {
          if (name != null) 'name': name,
          if (username != null) 'username': username,
          if (about != null) 'about': about,
          if (profilePicId != null) 'profilePicId': newProfilePicId,
          if (birthday != null) 'birthday': birthday,
          if (currency != null) 'currency': currency,
        });

    // if username != null the appwrite user name must be updated too
    if (username != null) {
      await _auth.updateUserName(username);
    }

    notifyListeners();
    _auth.getCurrUser();
    return res;
  }

  //endregion

  //region PRESENTS-RELATED STUFF

  Present _getPresentFromDoc(Document doc) {
    return Present(
      id: doc.$id,
      title: doc.data['title'],
      price: doc.data['price'].toDouble(),
      priority: doc.data['priority'],
      link: doc.data['link'],
      donorUid: doc.data['donorUid'],
    );
  }

  /// Return the list of presents of the given user
  Future<List<Present>> getUserPresents(String uid) async {

    try {
      List<Document> presentsDocs = (await _db.listDocuments(
              databaseId: Constants.wishlistsDbID, collectionId: uid))
          .documents;

      return presentsDocs.map((doc) => _getPresentFromDoc(doc)).toList();
    }
    catch (e) {
      if (e is AppwriteException && e.type == "collection_not_found") {
        await _functions.createExecution(
          functionId: 'wishlist_coll_creator',
          data: _uid,
          xasync: false,
        );
        return List<Present>.empty(growable: true);
      }
      else {
        rethrow;
      }
    }
  }

  /// Add the given present to the current user's ones
  Future<void> addPresent(Present present) async {

    try {
      // Create a new document for the passed present in the user's wishlist coll
      await _db.createDocument(
        databaseId: Constants.wishlistsDbID,
        collectionId: _uid,
        documentId: present.id,
        data: present.toJson(),
      );
    }
    catch (e) {
      // If the collection doesn't exists, create it and retry
      if (e is AppwriteException && e.type == "collection_not_found") {
        await _functions.createExecution(
          functionId: 'wishlist_coll_creator',
          data: _uid,
          xasync: false,
        );
        await addPresent(present);
      }
      else {
        rethrow;
      }
    }

    // notifyListeners();
  }

  /// Update an existing present with its new version provided
  /// NOTE: it can be also someone else's present where the donorUid has been modified
  Future<void> updatePresent(Present present, {String? presentOwnerUid, bool linkUpdated = false}) async {
    presentOwnerUid ??= _uid;

    try {
      await _db.updateDocument(
        databaseId: Constants.wishlistsDbID,
        collectionId: presentOwnerUid,
        documentId: present.id,
        data: present.toJson(),
      );
    }
    catch (e) {
      // If the collection doesn't exists, create it and retry
      if (e is AppwriteException && e.type == "collection_not_found") {
        await _functions.createExecution(
          functionId: 'wishlist_coll_creator',
          data: _uid,
          xasync: false,
        );
        await updatePresent(present, presentOwnerUid: presentOwnerUid);
      }
      else {
        rethrow;
      }
    }

    // If the link of the Present has been updated...
    if (linkUpdated) {
      // check if there was a cached image for the present and, in case there was, remove it
      SharedPreferences shPrefs = await SharedPreferences.getInstance();
      if (shPrefs.getString('${present.id}_image') != null) {
        await shPrefs.remove('${present.id}_image');
      }
    }

    notifyListeners();
  }

  /// Delete current user's present with the given id
  Future<void> deletePresent(String presentID) async {

    await _db.deleteDocument(
      databaseId: Constants.wishlistsDbID,
      collectionId: _uid,
      documentId: presentID
    );

    // Remove the cached image of this Present if it existed
    SharedPreferences shPrefs = await SharedPreferences.getInstance();
    if (shPrefs.getString('${presentID}_image') != null) {
      await shPrefs.remove('${presentID}_image');
    }

    notifyListeners();
  }

  //endregion

  //region STORAGE-RELATED STUFF

  /// Upload the given file as a new profile pic for the current user
  Future<void> uploadProfilePic(XFile file) async {
    String? oldPicId = _auth.currUser!.profilePicId;

    // convert the passed file into a List of bytes (required to work in Flutter Web)
    List<int> fileBytes = await file.readAsBytes();

    // upload the given file
    File res = await _storage.createFile(
      bucketId: Constants.profilePicsBucketID,
      fileId: 'unique()',
      file: InputFile(
        path: file.path,
        bytes: fileBytes,
        filename: '$_uid.${file.path.split('.').last}',
      ),
    );

    // update the picId property of the current user
    await updateUser(profilePicId: res.$id);

    // delete the old profile pic if it exists
    if (oldPicId != null) {
      await _storage.deleteFile(
          bucketId: Constants.profilePicsBucketID, fileId: oldPicId);
    }
  }

  /// Delete the current user's profile pic
  Future<void> removeProfilePic() async {
    await _storage.deleteFile(
      bucketId: Constants.profilePicsBucketID,
      fileId: _auth.currUser!.profilePicId!,
    );
    updateUser(profilePicId: 'null');
  }

  //endregion
}
