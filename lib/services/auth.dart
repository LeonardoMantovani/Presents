/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:appwrite/appwrite.dart' as aw;
import 'package:appwrite/models.dart';
import 'package:flutter/material.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/constants.dart';
import 'package:presents/utils/routing_utils.dart';

/// Class that handles all Authentication related tasks
/// and keeps track of the user currently logged in
class AuthService extends ChangeNotifier {
  // Constructor
  AuthService() {
    _client = aw.Client();

    _client
        .setEndpoint(Constants.awEndpoint)
        .setProject(Constants.awProjectID);

    _account = aw.Account(_client);

    //get current user
    // getCurrUser();
  }

  late aw.Client _client;
  late aw.Account _account;
  bool? _authenticated;
  bool _initialized = false;
  MyUser? _currUser;

  aw.Client get client => _client;
  MyUser? get currUser => _currUser;
  bool? get authenticated => _authenticated;
  bool get initialized => _initialized;

  Future<bool> initialize() async {
    try {
      // store current user in a temporary User variable
      // (if user isn't logged in this will throw an error and enter the catch block)
      Account u = await _account.get();
      // create a temporary DatabaseService instance
      DatabaseService db = DatabaseService(authService: this, uid: u.$id);
      // use that instance to get user data and store them in the MyUser variable
      _currUser = await db.getUserData();
      _authenticated = true;
      _initialized = true;
      return true;
    }
    on aw.AppwriteException {
      // if the try block fails the user isn't logged in so set _currUser to null
      _currUser = null;
      _authenticated = false;
      _initialized = true;
      return false;
    }
  }

  /// Update the currUser variable of this class with the current user and then
  /// notify listeners
  Future<void> getCurrUser() async {
    try {
      // store current user in a temporary User variable
      // (if user isn't logged in this will throw an error and enter the catch block)
      Account u = await _account.get();
      // create a temporary DatabaseService instance
      DatabaseService db = DatabaseService(authService: this, uid: u.$id);
      // use that instance to get user data and store them in the MyUser variable
      _currUser = await db.getUserData();
      _authenticated = true;
    }
    on aw.AppwriteException {
      // if the try block fails the user isn't logged in so set _currUser to null
      _currUser = null;
      _authenticated = false;
    }
    finally {
      notifyListeners();
    }
  }

  /// Log the user in.
  /// Returns null if succeeded or a string with the error in case of failure
  Future<String?> logIn({ required String email, required String password }) async {
    try {
      // create a session for the existing user
      await _account.createEmailSession(email: email, password: password);
      // save the current session in its variable and notify listeners
      await getCurrUser();

      return null;
    }
    catch (e) {
      return e.toString();
    }
  }

  /// return null if reset email was sent correctly, a string with the error in case of failure
  Future<String?> createPasswordRecovery({ required String email }) async {
    try {
      await _account.createRecovery(
        email: email,
        url: "https://presents.lezsoft.com${routePath(RouteCodes.pswReset)}"
      );

      return null;
    }
    catch (e) {
      return e.toString();
    }
  }

  Future<String?> resetPassword(String userId, String userSecret, String newPassword) async {

    // Remove unnecessary spaces in the userSecret (if any)
    userSecret = userSecret.replaceAll(' ', '');

    try {
      await _account.updateRecovery(
        userId: userId,
        secret: userSecret,
        password: newPassword,
        passwordAgain: newPassword
      );
      return null;
    }
    catch (e) {
      return e.toString();
    }
  }

  Future<String?> updateUserName(String newName) async {
    try {
      await _account.updateName(name: newName);
      return null;
    }
    catch (e) {
      return e.toString();
    }
  }

  /// Create a new user (with its document in the database) and log him in.
  /// return null if succeeded or a string with the error if it failed
  Future<String?> signUp({required String email, required String username, required String password}) async {
    try {
      // create a new AppWrite User
      Account u = await _account.create(userId: 'unique()', name: username ,email: email, password: password);
      // create a session for the new user
      await _account.createEmailSession(email: email, password: password);

      // create a temporary DatabaseService instance
      DatabaseService db = DatabaseService(authService: this, uid: u.$id);
      // create a new UserData document for the new user
      await db.createUserData(uid: u.$id, email: email, username: username);

      // save the current session in its variable and notify listeners
      await getCurrUser();

      return null;
    }
    catch (e) {
      return e.toString();
    }
  }

  /// Log the user out
  Future<String?> logOut() async {
    try {
      await _account.deleteSession(sessionId: 'current');
      await getCurrUser();
      return null;
    }
    catch (e) {
      return e.toString();
    }
  }
}