/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/models/present.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/default_text_form_field.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

/// Form to edit existing wishes or add new ones
class WishForm extends StatefulWidget {
  const WishForm({Key? key, required this.db, this.present, this.newPresentLink, this.doAfterSave}) : super(key: key);

  final DatabaseService db;
  final Present? present;
  final String? newPresentLink;
  final Function? doAfterSave;

  @override
  State<WishForm> createState() => _WishFormState();
}

class _WishFormState extends State<WishForm> {

  late DatabaseService _db;

  final _formKey = GlobalKey<FormState>();
  late final TextEditingController _title;
  late final TextEditingController _price;
  int? _priority;
  late final TextEditingController _link;

  final double _space = 20.0;
  IconData? currencyIcon; 

  @override
  void initState() {
    _db = widget.db;
    _title = TextEditingController(text: widget.present?.title);
    _price = TextEditingController(text: widget.present?.price.toString());
    _link = TextEditingController(text: widget.present?.link ?? widget.newPresentLink);
    super.initState();
  }

  IconData _getUserCurrency(BuildContext context) {
    AuthService auth = Provider.of<AuthService>(context, listen: false);
    switch(auth.currUser?.currency) {
      case '€':
        return Icons.euro;
      case '\$':
        return Icons.attach_money;
      case '£':
        return Icons.currency_pound;
      case '₹':
        return Icons.currency_rupee;
      case '¥':
        return Icons.currency_yen;
      case '₽':
        return Icons.currency_ruble;
      default:
        return Icons.euro;
    }
  }

  @override
  Widget build(BuildContext context) {

    Present? present = widget.present;
    _priority ??= present?.priority;

    ColorScheme themeColors = Theme.of(context).colorScheme;

    currencyIcon ??= _getUserCurrency(context); 

    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          //region HEADING
          Text(
            (present == null) ? 'New Wish' : 'Editing: ${present.title}',
            style: const TextStyle(fontSize: 18.0),
          ),
          SizedBox(height: _space*1.5,),
          //endregion

          //region TITLE FIELD
          DefaultTextFormField(
            controller: _title,
            icon: Icons.title,
            label: 'Title',
            validator: (value) => (value != null && value.isNotEmpty) ? null : 'Please choose a title',
          ),
          SizedBox(height: _space,),
          //endregion

          //region LINK FIELD
          DefaultTextFormField(
            controller: _link,
            icon: Icons.link,
            label: 'Link',
            // initialValue: present?.link,
            validator: (value) {
              if (value != null && value.isNotEmpty) {
                if (value.startsWith('http://') || value.startsWith('https://')) {
                  return null;
                }
                else {
                  return 'Please provide a valid URL (https://...)';
                }
              }
              else {
                return null;
              }
            },
          ),
          SizedBox(height: _space,),
          //endregion

          //region PRICE FIELD
          DefaultTextFormField(
            controller: _price,
            icon: currencyIcon!,
            label: 'Price',
            keyboardType: TextInputType.number,
            // initialValue: '${present?.price}',
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter a price';
              }
              else if (double.tryParse(value) == null) {
                return 'Please enter a valid price';
              }
              else {
                return null;
              }
            }
          ),
          SizedBox(height: _space*1.5,),
          //endregion

          //region PRIORITY SLIDER
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const Text(' Priority', style: TextStyle(fontSize: 16.0,),),
              SizedBox(
                width: 200,
                child: Slider(
                  min: 1,
                  max: 10,
                  divisions: 9,
                  label: 'Priority',
                  value: (_priority ?? 1).toDouble(),
                  onChanged: (value) {
                    setState(() => _priority = value.toInt());
                  },
                ),
              )
            ],
          ),
          SizedBox(height: _space*2,),
          //endregion

          //region SAVE BUTTON
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              if (widget.doAfterSave != null)
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: themeColors.onBackground,
                    foregroundColor: themeColors.background,
                  ),
                  onPressed: () {
                    if (widget.doAfterSave == null) {
                      Navigator.pop(context);
                    }
                    else {
                      widget.doAfterSave!();
                    }
                  },
                  child: const Text('CANCEL')
                ),
              ElevatedButton(
                child: const Text('SAVE'),
                onPressed: () async {
                  if (_formKey.currentState?.validate() ?? false) {
                    if (present == null) {
                      Present newPresent = Present(
                        id: const Uuid().v1(),
                        title: _title.text,
                        link: (_link.text != '') ? _link.text : null,
                        price: double.parse(double.parse(_price.text).toStringAsFixed(2)),
                        priority: _priority ?? 1,
                      );
                      await _db.addPresent(newPresent);
                    }
                    else {
                      Present updatedPresent = Present(
                        id: present.id,
                        title: _title.text,
                        link: (_link.text != '') ? _link.text : null,
                        price: double.parse(double.parse(_price.text).toStringAsFixed(2)),
                        priority: _priority ?? 1,
                      );
                      await _db.updatePresent(updatedPresent, linkUpdated: _link.text != '');
                    }
                    if (widget.doAfterSave == null) {
                      if (!mounted) return;
                      Navigator.of(context).pop();
                    }
                    else {
                      widget.doAfterSave!();
                    }
                  }
                },
              ),
            ],
          ),
          SizedBox(height: _space,),
          //endregion

        ],
      ),
    );
  }
}
