/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/screens/wishlist/presents_list.dart';
import 'package:presents/screens/wishlist/wish_form.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/constants.dart';
import 'package:presents/utils/default_appbar.dart';
import 'package:presents/screens/wishlist/show_wish_panel.dart';
import 'package:presents/utils/global_shared_link.dart';
import 'package:presents/utils/navbar_vert.dart';
import 'package:provider/provider.dart';

/// Wishlist Screen
class WishList extends StatefulWidget {
  const WishList({Key? key}) : super(key: key);

  @override
  State<WishList> createState() => _WishListState();
}

class _WishListState extends State<WishList> {

  Widget? _detailsPanel;

  @override
  Widget build(BuildContext context) {

    AuthService auth = Provider.of<AuthService>(context);

    ColorScheme themeColors = Theme.of(context).colorScheme;

    void setDetailsPanel(Widget? gottenPanel) {
      setState(() => _detailsPanel = gottenPanel);
    }

    _detailsPanel ??= Text(
        'No Wish selected yet,\nCick on an existing one to edit it\nor click the + button to add a new one',
        style: TextStyle(
          fontStyle: FontStyle.italic,
          fontSize: 16.0,
          color: themeColors.onBackground,
        ),
        textAlign: TextAlign.center,
      );

    if (globalSharedLink != null) {

      DatabaseService db = Provider.of<DatabaseService>(context, listen: false);

      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        String newPresentLink = globalSharedLink!;
        globalSharedLink = null;

        if (MediaQuery.of(context).size.width <= Constants.maxPortraitWidth) {
          showNewWishPanel(context, db, newPresentLink: newPresentLink);
        }
        else {
          setState(() {
            _detailsPanel = WishForm(
              db: db,
              newPresentLink: newPresentLink,
              doAfterSave: () => setState(() => _detailsPanel = null),
            );
          });
        }
      });
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        //region APP BAR
        DefaultAppBar(
          title: 'WishList',
          actions: <Widget>[
            Consumer<DatabaseService>(
                builder: (context, db, child) {
                  return IconButton(
                    icon: const Icon(Icons.add, size: 28.0,),
                    onPressed: () {
                      if (MediaQuery.of(context).size.width <= Constants.maxPortraitWidth) {
                        showNewWishPanel(context, db);
                      }
                      else {
                        setState(() => _detailsPanel = WishForm(db: db, doAfterSave: () => setState(() => _detailsPanel = null),));
                      }
                    },
                  );
                }
            ),
          ],
        ),
        //endregion

        //region BODY
        Expanded(
          child: LayoutBuilder(
            builder: (context, constraints) {
              // display just the Presents List if in Portrait mode
              if (constraints.maxWidth <= Constants.maxPortraitWidth) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: PresentsList(
                    presentsOwner: auth.currUser!,
                    ownerView: true,
                  ),
                );
              }
              // or the NavBar + PresentsList + DetailsPanel if in Landscape mode
              else {
                return Row(
                  children: [
                    // region NAVBAR
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                      child: LayoutBuilder(
                          builder: (context, constraints) {
                            if (constraints.maxHeight <= 500) {
                              return vertNavBar(context, 0);
                            }
                            else {
                              return SizedBox(height: 500, child: vertNavBar(context, 0));
                            }
                          }
                      ),
                    ),
                    //endregion

                    //region PRESENTS LIST
                    Flexible(
                      flex: 4,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: PresentsList(
                          presentsOwner: auth.currUser!,
                          ownerView: true,
                          detailsPanelSetter: setDetailsPanel,
                        ),
                      ),
                    ),
                    //endregion

                    //region DETAILS PANEL
                    Flexible(
                      flex: 3,
                      child: Padding(
                        padding: const EdgeInsets.all(50.0),
                        child: LayoutBuilder(
                          builder: (context, constraints) {
                            return Card(
                              elevation: 10.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)
                              ),
                              color: themeColors.background,
                              child: Container(
                                height: constraints.maxHeight,
                                width: constraints.maxWidth,
                                margin: const EdgeInsets.all(25.0),
                                child: Center(
                                  child: SingleChildScrollView(child: _detailsPanel,),
                                ),
                              ),
                            );
                          }
                        )
                      ),
                    ),
                    //endregion
                  ],
                );
              }
            }
          ),
        ),
        //endregion
      ],
    );
  }
}
