/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'dart:convert';
import 'dart:typed_data';

import 'package:appwrite/appwrite.dart';
import 'package:appwrite/models.dart';
import 'package:flutter/material.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/models/present.dart';
import 'package:presents/screens/wishlist/wish_form.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/random_colored_box.dart';
import 'package:presents/screens/wishlist/show_wish_panel.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

/// Widget used to show a Present in a list of results, displaying:
/// - Preview Image fetched from the link (if it exists)
/// - Title of the present
/// - Price of the present
/// - a button to visit the present's link (if it exists)
/// - a button to reserve the present (if it hasn't already been reserved)
/// - a button to un-reserve the present (if it had been reserved by the current user)
class PresentCard extends StatelessWidget {
  // Constructor
  const PresentCard({
    Key? key,
    required this.present,
    this.ownerView = false,
    this.guestView = false,
    required this.presentOwner,
    required this.db,
    this.detailsPanelSetter,
  }) : super(key: key);

  final Present present;
  final bool ownerView;
  final bool guestView;
  final MyUser presentOwner;
  final DatabaseService db;
  final Function? detailsPanelSetter;

  /// Method which returns a row of actions to display on the card,
  /// based on who is viewing the present and who reserved it
  Widget _getTrailing(context, MyUser user, {bool onReserved=false}) {

    Color actionColor = Theme.of(context).colorScheme.onTertiary;

    List<Widget> children = List<Widget>.empty(growable: true);

    //region OWNER ACTIONS
    if (ownerView) {
      //region Link Button if needed
      if (present.link != null) {
        children.add(IconButton(
            icon: Icon(
              Icons.link,
              color: actionColor,
            ),
            onPressed: () async {
              if (await canLaunchUrl(Uri.parse(present.link!))) {
                await launchUrl(Uri.parse(present.link!), mode: LaunchMode.externalApplication);
              }
            }
        ));
      }
      //endregion
      //region Popup Menu Button (to edit/delete the present)
      children.add(PopupMenuButton(
        onSelected: (String value) {
          if (value == 'edit') {
            if (detailsPanelSetter == null) {
              showNewWishPanel(context, db, present: present);
            }
            else {
              detailsPanelSetter!(WishForm(db: db, present: present, doAfterSave: () => detailsPanelSetter!(null),));
            }
          } else {
            db.deletePresent(present.id);
            if (detailsPanelSetter != null) {
              detailsPanelSetter!(null);
            }
          }
        },
        icon: Icon(Icons.more_vert, color: actionColor,),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          const PopupMenuItem<String>(value: 'edit', child: Text('Edit')),
          const PopupMenuItem<String>(value: 'delete', child: Text('Delete')),
        ],
      ));
      //endregion
    }
    //endregion

    //region GUEST ACTIONS
    else if (guestView) {
      // if I'm on the 'original' card (not the reserved/gray one)...
      if (!onReserved) {
        // ...show Link button and reserve button only if the present hasn't
        // been reserved yet (and so the reserved/gray card won't be shown)
        // NOTE: instead of not building the icons at all, if the present has
        // been reserved they will be built with a transparent color, in order
        // to preserve the same card size before and after reservation
        Color iconColor = present.donorUid == null ? actionColor : Colors.transparent;
        //region Link Button if needed
        if (present.link != null) {
          children.add(IconButton(
              icon: Icon(
                Icons.link,
                color: iconColor,
              ),
              onPressed: () async {
                if (await canLaunchUrl(Uri.parse(present.link!))) {
                  await launchUrl(Uri.parse(present.link!), mode: LaunchMode.externalApplication);
                }
              }
          ));
        }
        //endregion
        //region Reserve Button
        children.add(IconButton(
            icon: Icon(Icons.check, color: iconColor,),
            onPressed: () {
              // set the donorUid of this present to current user's uid
              Present updatedPresent = present;
              updatedPresent.donorUid = user.uid;
              db.updatePresent(updatedPresent, presentOwnerUid: presentOwner.uid);
            }
        ));
        //endregion
      }
      // else (I'm on the reserved/grey card)...
      else {
        //region Link Button if needed
        if (present.link != null) {
          children.add(IconButton(
              icon: Icon(
                Icons.link,
                color: actionColor,
              ),
              onPressed: () async {
                if (await canLaunchUrl(Uri.parse(present.link!))) {
                  await launchUrl(Uri.parse(present.link!), mode: LaunchMode.externalApplication);
                }
              }
          ));
        }
        //endregion
        if (present.donorUid == user.uid) {
          //region Un-reserve Button
          children.add(IconButton(
            icon: Icon(Icons.close, color: actionColor,),
            onPressed: () {
              // set the donorUid of this present back to null
              Present updatedPresent = present;
              updatedPresent.donorUid = null;
              db.updatePresent(updatedPresent, presentOwnerUid: presentOwner.uid);
            },
          ));
          //endregion
        }
      }

    }
    //endregion

    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: children,
    );
  }

  /// A method to get the Present's link preview image using an AppWrite function
  /// (or loading it from shared_preferences if previously cached)
  Future<Widget> _getImage(BuildContext context) async {
    // Get the Appwrite Function service
    Client awClient = Provider.of<AuthService>(context, listen: false).client;
    Functions awFunctions = Functions(awClient);
    // Get the cached image for this present from shared_preferences (or null if it doesn't exist)
    SharedPreferences shPrefs = await SharedPreferences.getInstance();
    String? cachedImage = shPrefs.getString('${present.id}_image');

    // If the present has no link simply return a RandomColoredBox
    if (present.link == null) {
      return const RandomColoredBox();
    }
    // else if an image for this present has already been cached before, return it
    else if (cachedImage != null) {
      Uint8List imageBytes = base64Decode(cachedImage);
      return InkWell(
        onTap: () async {
          if (await canLaunchUrl(Uri.parse(present.link!))) {
            await launchUrl(Uri.parse(present.link!), mode: LaunchMode.externalApplication);
          }
        },
        borderRadius: BorderRadius.circular(15.0),
        child: Image.memory(
          imageBytes,
          fit: BoxFit.cover,
        ),
      );
    }
    // Else download the image using Appwrite functions
    else {
      try {
        // Invoke the Appwrite 'image_fetcher' function
        Execution result = await awFunctions.createExecution(
          functionId: 'image_fetcher',
          data: present.link,
          xasync: false,
        );

        // Save its response into a json map
        Map<String, dynamic> response = jsonDecode(result.response);

        // Decode the image's bytes provided by the response
        Uint8List imageBytes = base64Decode(response['encoded_image']);

        // Save the returned image in shared_preferences (encoded as a String)
        await shPrefs.setString('${present.id}_image', response['encoded_image']);

        // Create an Image widget with the imageBytes that will be returned
        return InkWell(
          onTap: () async {
            if (await canLaunchUrl(Uri.parse(present.link!))) {
              await launchUrl(Uri.parse(present.link!), mode: LaunchMode.externalApplication);
            }
          },
          borderRadius: BorderRadius.circular(15.0),
          child: Image.memory(
            imageBytes,
            fit: BoxFit.cover,
          ),
        );
      }
      catch (e) {
        // If there has been an error with the image fetching, return a RandomColoredBox
        return const RandomColoredBox();
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    ColorScheme themeColors = Theme.of(context).colorScheme;

    return Stack(
      children: [
        Card(
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          color: themeColors.tertiary,
          margin: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 7.0),
          child: Consumer<AuthService>(
              builder: (context, auth, child) {
                return ListTile(
                  leading: ClipRRect(
                    borderRadius: BorderRadius.circular(15.0),
                    child: FutureBuilder(
                      future: _getImage(context),
                      builder: (context, snapshot) {
                        return Container(
                          width: 45.0,
                          height: 45.0,
                          color: Theme.of(context).colorScheme.tertiary,
                          child: snapshot.data,
                        );
                      }
                    ),
                  ),
                  title: Text(present.title),
                  subtitle: Text('${presentOwner.currency}${present.price.toStringAsFixed(2)}'),
                  trailing: _getTrailing(context, auth.currUser!),
                );
              }
          ),
        ),
        if (guestView && present.donorUid!=null) Opacity(
          opacity: 0.8,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            color: Colors.grey,
            margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 7.0),
            child: Consumer<AuthService>(
              builder: (context, auth, child) {
                return ListTile(
                  leading: ClipRRect(
                    borderRadius: BorderRadius.circular(15.0),
                    child: const SizedBox(
                      width: 45.0,
                      height: 45.0,
                    ),
                  ),
                  title: Text(present.title, style: const TextStyle(color: Colors.transparent),),
                  subtitle: const Text(''),
                  trailing: _getTrailing(context, auth.currUser!, onReserved: true),
                );
              }
            ),
          ),
        ),
      ],
    );
  }
}
