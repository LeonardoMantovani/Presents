/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/models/present.dart';
import 'package:presents/screens/wishlist/present_card.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/loading.dart';
import 'package:presents/utils/two_side_sliver.dart';
import 'package:provider/provider.dart';

class PresentsGrid extends StatelessWidget {
  // Constructor
  const PresentsGrid({
    Key? key,
    this.ownerView = false,
    this.guestView = false,
    required this.presentsOwner,
    this.detailsPanelSetter,
  }) : super(key: key);

  final bool ownerView;
  final bool guestView;
  final MyUser presentsOwner;
  final Function? detailsPanelSetter;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: Consumer<DatabaseService>(
        builder: (context, db, child) {
          return FutureBuilder(
              future: db.getUserPresents(presentsOwner.uid),
              builder: (context, snapshot) {
                // return Loading until database request has finished
                if (!snapshot.hasData) {
                  return const Loading();
                }
                else {
                  // get the presents list
                  List<Present> presents = snapshot.data as List<Present>;

                  // sort them by priority
                  presents.sort((a, b) => b.priority.compareTo(a.priority));

                  // divide the (sorted) presents in two lists (one for each column)
                  List<Present> leftColPresents = List<Present>.empty(growable: true);
                  List<Present> rightColPresents = List<Present>.empty(growable: true);
                  for (int i=0; i < presents.length; i++) {
                    if (i % 2 == 0) {
                      leftColPresents.add(presents[i]);
                    }
                    else {
                      rightColPresents.add(presents[i]);
                    }
                  }

                  // return a 2-columns list of Presents Cards (or a text if the list is empty)
                  return (presents.isEmpty)
                    ? const Center(child: Text('No Wishes added yet'))
                    : CustomScrollView(
                      slivers: [
                        TwoSideSliver(
                          leftSize: MediaQuery.of(context).size.width / 2,
                          left: SliverList(
                            delegate: SliverChildBuilderDelegate(
                              childCount: leftColPresents.length,
                                  (context, index) => PresentCard(
                                present: leftColPresents[index],
                                ownerView: ownerView,
                                guestView: guestView,
                                presentOwner: presentsOwner,
                                db: db,
                                detailsPanelSetter: detailsPanelSetter,
                              ),
                            ),
                          ),
                          right: SliverList(
                            delegate: SliverChildBuilderDelegate(
                              childCount: rightColPresents.length,
                                  (context, index) => PresentCard(
                                present: rightColPresents[index],
                                ownerView: ownerView,
                                guestView: guestView,
                                presentOwner: presentsOwner,
                                db: db,
                                detailsPanelSetter: detailsPanelSetter,
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                }
              }
          );
        },
      ),
    );
  }
}
