/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/screens/profile/profile.dart';
import 'package:presents/screens/search/search.dart';
import 'package:presents/screens/wishlist/wishlist.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/constants.dart';
import 'package:presents/utils/navbar_orizz.dart';
import 'package:provider/provider.dart';

/// Widget which will contain the Wishlist/Search/Profile screen and let navigate
/// between them using a customized bottom navigation bar
class Home extends StatefulWidget {
  const Home({Key? key, required this.index}) : super(key: key);

  final int index;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  late Widget _body;

  @override
  Widget build(BuildContext context) {

    AuthService auth = Provider.of<AuthService>(context);

    // set the body widget
    switch (widget.index) {
      case 0:
        _body = const WishList();
        break;
      case 1:
        _body = const Search();
        break;
      case 2:
        _body = Profile(auth: auth,);
        break;
      default:
        _body = const WishList();
        break;
    }

    return Scaffold(
      body: SafeArea(
        child: ChangeNotifierProvider(
          create: (context) => DatabaseService(authService: auth, uid: auth.currUser!.uid),
          child: Center(
            child: _body,
          ),
        ),
      ),
      bottomNavigationBar: MediaQuery.of(context).size.width > Constants.maxPortraitWidth ? null : Padding(
        padding: const EdgeInsets.symmetric(
            horizontal: 10.0, vertical: 20.0),
        child: orizzNavBar(context, widget.index),
      ),
    );
  }
}
