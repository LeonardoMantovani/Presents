/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:presents/utils/custom_button_style.dart';
import 'package:presents/utils/default_appbar.dart';

class PageNotFound extends StatelessWidget {
  const PageNotFound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              DefaultAppBar(title: '404: Page Not Found'),
              const SizedBox(height: 25.0,),
              const Text(
                "Whooops! We couldn't find the page you requested!\n"
                  "Please double-check your URL",
                style: TextStyle(fontSize: 30.0),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 25.0,),
              ElevatedButton.icon(
                style: customButtonStyle(),
                onPressed: () => context.go('/'),
                label: const Text('BACK TO HOMEPAGE',),
                icon: const Icon(Icons.home,),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
