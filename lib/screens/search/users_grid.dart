/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/screens/search/user_card.dart';

class UsersGrid extends StatelessWidget {
  const UsersGrid({Key? key, required this.users, required this.emptyText}) : super(key: key);

  final List<MyUser> users;
  final String emptyText;

  @override
  Widget build(BuildContext context) {
    if (users.isEmpty) {
      return Center(child: Text(emptyText),);
    }
    else {

      // Check if there is a divider in the users list and handle it
      // (dividers must be a 2-cells-long line if their index is odd)
      for (int i = 0; i < users.length; i++) {
        if (users[i].uid == 'divider') {
          if (i % 2 == 0) {
            users.removeAt(i);
            users.insert(i, MyUser.fakeUser('dividerSX'));
            users.insert(i+1, MyUser.fakeUser('dividerDX'));
          }
          else {
            users.insert(i+1, MyUser.fakeUser('dividerSX'));
            users.insert(i+2, MyUser.fakeUser('dividerDX'));
          }
          break;
        }
      }

      return Padding(
        padding: const EdgeInsets.only(top: 15.0),
        child: GridView.builder(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisExtent: 75.0,
            ),
            itemCount: users.length,
            itemBuilder: (context, index) {
              switch (users[index].uid) {
                case 'divider':
                  return const SizedBox();
                case 'dividerSX':
                  return const Divider(indent: 20.0);
                case 'dividerDX':
                  return const Divider(endIndent: 20.0);
                default:
                  return UserCard(user: users[index]);
              }
            }
        ),
      );
    }
  }
}
