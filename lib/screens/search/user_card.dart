/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:photo_view/photo_view.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/utils/random_colored_box.dart';
import 'package:presents/utils/routing_utils.dart';
import 'package:provider/provider.dart';

/// Widget used to show a user in a list of results, displaying:
/// - it's profile pic (which acts as a button to his profile page too)
/// - it's name and username
/// - a button to his wishlist page
class UserCard extends StatefulWidget {
  const UserCard({Key? key, required this.user}) : super(key: key);

  final MyUser user;

  @override
  State<UserCard> createState() => _UserCardState();
}

class _UserCardState extends State<UserCard> {
  @override
  Widget build(BuildContext context) {

    ColorScheme themeColors = Theme.of(context).colorScheme;

    MyUser user = widget.user;

    return Card(
      elevation: 3,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      color: themeColors.tertiary,
      margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 7.0),
      child: InkWell(
        onTap: () {
          context.goNamed(routeName(RouteCodes.otherProfile), params: {'uid': user.uid});
        },
        borderRadius: BorderRadius.circular(15.0),
        child: ListTile(
          leading: Consumer<AuthService>(
            builder: (context, auth, child) {
              if (user.profilePic != null) {
                return InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => Material(
                          child: SafeArea(
                            child: Stack(
                              children: [
                                PhotoView(imageProvider: user.profilePic!.image),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 25.0),
                                    child: IconButton(
                                        onPressed: () =>
                                            Navigator.of(context).pop(),
                                        icon: const Icon(
                                          Icons.close,
                                          size: 48,
                                          color: Colors.grey,
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                    ));
                  },
                  borderRadius: BorderRadius.circular(15.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15.0),
                    child: Container(
                      width: 45.0,
                      height: 45.0,
                      color: themeColors.tertiary,
                      child: user.profilePic ?? const RandomColoredBox(),
                    ),
                  ),
                );
              }
              else {
                return ClipRRect(
                  borderRadius: BorderRadius.circular(15.0),
                  child: Container(
                    width: 45.0,
                    height: 45.0,
                    color: themeColors.tertiary,
                    child: user.profilePic ?? const RandomColoredBox(),
                  ),
                );
              }
            }
          ),
          title: Text(user.name ?? '@${user.username}'),
          subtitle: Text((user.name!=null) ? '@${user.username}' : ''),
          trailing: Consumer<AuthService>(
            builder: (context, auth, child) {
              return IconButton(
                icon: Icon(
                  Icons.format_list_bulleted,
                  color: themeColors.onTertiary,
                  size: 28.0,
                ),
                onPressed: () {
                  context.goNamed(routeName(RouteCodes.otherWishlist), params: {'uid': user.uid});
                },
              );
            }
          ),
        ),
      ),
    );
  }
}
