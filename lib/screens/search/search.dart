/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/screens/search/search_appbar.dart';
import 'package:presents/screens/search/users_grid.dart';
import 'package:presents/screens/search/users_list.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/constants.dart';
import 'package:presents/utils/loading.dart';
import 'package:presents/utils/navbar_vert.dart';
import 'package:provider/provider.dart';

/// Search Screen
class Search extends StatefulWidget {
  const Search({Key? key}) : super(key: key);

  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {

  FocusNode focusNode = FocusNode();
  bool _isSearching = false;
  String _searchedText = '';

  /// method which updates the variable used to see if the user is currently
  /// searching something or not
  void updateSearchingState (String? searchedText) {
    if (searchedText == null) {
      setState(() => _isSearching = false);
    }
    else {
      setState(() {
        _searchedText = searchedText;
        _isSearching = true;
      });
    }
  }

  /// method that returns a list of users matching the given searchedText.
  /// The method will return both the ones matching for the username and the ones
  /// matching only for the name, divided by a fake-user with uid='divider'
  Future<List<MyUser>> _search(String searchedText, DatabaseService db) async {
    List<MyUser> usernameMatchingUsers = await db.searchUser(searchedText, byUsername: true);
    List<MyUser> nameMatchingUsers = await db.searchUser(searchedText, byUsername: false);
    List<String> nameMatchingUsersUids = nameMatchingUsers.map((u) => u.uid).toList();
    List<MyUser> results = List<MyUser>.empty(growable: true);

    // create a fake user to use as divider between results
    MyUser divider = MyUser.fakeUser('divider');

    // join the 2 lists and the divider in one final list and return it
    // (since AppWrite consider spaces in the string as a divider between
    // different queries instead of a character, before adding the user to the
    // results check that it is a valid result)
    for (MyUser user in usernameMatchingUsers) {
      bool wrongResult = !user.username.toLowerCase().contains(searchedText.toLowerCase());
      if (!wrongResult) {
        results.add(user);
      }
      if (nameMatchingUsersUids.contains(user.uid) || wrongResult) {
        nameMatchingUsersUids.remove(user.uid);
      }
    }
    if (usernameMatchingUsers.isNotEmpty && nameMatchingUsersUids.isNotEmpty) {
      results.add(divider);
    }
    for (MyUser user in nameMatchingUsers) {
      bool wrongResult = !user.name!.toLowerCase().contains(searchedText.toLowerCase());
      if (!wrongResult && nameMatchingUsersUids.contains(user.uid)) {
        results.add(user);
      }
    }
    return results;
  }

  /// A method which returns the proper body based on what the user is doing:
  /// - if he isn't searching it will return a list of favourite users,
  /// - if he's searching it will return the search results
  Widget _getBody(BuildContext context, {bool landscape = false}) {
    if (_isSearching == false) {
      return Consumer<AuthService>(
          builder: (context, auth, child) {
            return landscape ? UsersGrid(
              users: auth.currUser!.favUsers,
              emptyText: 'No Favourite Users Found',
            ) : UsersList(
              users: auth.currUser!.favUsers,
              emptyText: 'No Favourite Users Found',
            );
          }
      );
    }
    else {
      if (_searchedText == '') {
        return const Text('');
      }
      else {
        return Consumer<DatabaseService>(
          builder: (context, db, child) {
            return FutureBuilder(
              future: _search(_searchedText, db),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return landscape ? UsersGrid(
                    users: snapshot.data!,
                    emptyText: "No users found.",
                  ) : UsersList(
                    users: snapshot.data!,
                    emptyText: 'No users found',
                  );
                }
                else {
                  return const Loading();
                }
              }
            );
          }
        );

      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        //region APP BAR
        SearchAppBar(updateParentSearchState: updateSearchingState, focusNode: focusNode,),
        //endregion

        Expanded(
          child: LayoutBuilder(
            builder: (context, constraints) {
              // display just the body if in Portrait mode
              if (constraints.maxWidth <= Constants.maxPortraitWidth) {
                return _getBody(context);
              }
              // or the NavBar + the Body if in Landscape mode
              else {
                return Row(
                  children: [
                    //region NAVBAR
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                      child: LayoutBuilder(
                          builder: (context, constraints) {
                            if (constraints.maxHeight <= 500) {
                              return vertNavBar(context, 1);
                            }
                            else {
                              return SizedBox(height: 500, child: vertNavBar(context, 1));
                            }
                          }
                      ),
                    ),
                    //endregion
                    
                    //region BODY
                    Expanded(child: _getBody(context, landscape: true)),
                    //endregion
                  ],
                );
              }
            },
          ),
        ),
      ],
    );
  }
}
