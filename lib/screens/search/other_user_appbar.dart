/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/routing_utils.dart';
import 'package:provider/provider.dart';

/// Customized AppBar for the OtherUser's Wishlist and Profile pages
class OtherUserAppBar extends StatefulWidget {
  // Constructor
  const OtherUserAppBar({
    Key? key,
    required this.title,
    required this.otherUser,
    required this.isAlreadyFavourite,
    required this.wishlistView,
  }) : super(key: key);

  final String title;
  final MyUser otherUser;
  final bool isAlreadyFavourite;
  final bool wishlistView;

  @override
  State<OtherUserAppBar> createState() => _OtherUserAppBarState();
}

class _OtherUserAppBarState extends State<OtherUserAppBar> {

  late bool _isFavourite;

  void _changeFavState(DatabaseService db) async {
    if (_isFavourite) {
      db.removeFavUser(widget.otherUser);
      setState(() => _isFavourite = false);
    }
    else {
      await db.addFavUser(widget.otherUser);
      setState(() => _isFavourite = true);
    }
  }

  @override
  void initState() {
    super.initState();
    _isFavourite = widget.isAlreadyFavourite;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        // leading:
        titleSpacing: 0.0,
        title: FittedBox(
          fit: BoxFit.fitWidth,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(widget.title),
              Consumer<DatabaseService>(
                  builder: (context, db, child) {

                    // Update the _isFavourite variable so that is synced with changes
                    // on other screens
                    AuthService auth = Provider.of<AuthService>(context);
                    bool isFavourite = false;
                    for (MyUser usr in auth.currUser!.favUsers) {
                      if (usr.uid == widget.otherUser.uid) {
                        isFavourite = true;
                      }
                    }
                    _isFavourite = isFavourite;

                    return IconButton(
                      onPressed: () => _changeFavState(db),
                      icon: Icon(_isFavourite ? Icons.star : Icons.star_border),
                    );
                  }
              ),
            ],
          ),
        ),
        actions: [
          // buttons to switch between Profile and Wishlist pages of the user
          if (widget.wishlistView)
            IconButton(
              onPressed: () => context.goNamed(routeName(RouteCodes.otherProfile), params: {'uid': widget.otherUser.uid}),
              icon: Icon(
                Icons.account_circle,
                color: Theme.of(context).colorScheme.onPrimary,
              ),
            ),
          if (!widget.wishlistView)
            IconButton(
              onPressed: () => context.goNamed(routeName(RouteCodes.otherWishlist), params: {'uid': widget.otherUser.uid}),
              icon: Icon(
                Icons.format_list_bulleted,
                color: Theme.of(context).colorScheme.onPrimary,
              ),
            ),
        ],
      ),
    );
  }
}