/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:photo_view/photo_view.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/screens/search/other_user_appbar.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/constants.dart';
import 'package:presents/utils/loading.dart';
import 'package:presents/utils/random_colored_box.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';

/// OtherUser's Profile page
class OtherUserProfile extends StatefulWidget {
  // Constructor
  const OtherUserProfile({
    Key? key, required this.authService, required this.otherUserUID
  }) : super(key: key);

  final AuthService authService;
  final String otherUserUID;

  @override
  State<OtherUserProfile> createState() => _OtherUserProfileState();
}

class _OtherUserProfileState extends State<OtherUserProfile> {

  bool _isAlreadyFavourite = false;

  @override
  void initState() {
    super.initState();

    // check if the other user is in the favourites list of the current user
    for (MyUser usr in widget.authService.currUser!.favUsers) {
      if (usr.uid == widget.otherUserUID) {
        _isAlreadyFavourite = true;
        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    AuthService auth = Provider.of<AuthService>(context);
    DatabaseService db = DatabaseService(authService: auth, uid: auth.currUser!.uid);

    return FutureBuilder(
      future: db.getUserData(userId: widget.otherUserUID),
      builder: (context, snapshot) {
        // While the future isn't completed, show the loading indicator
        if (snapshot.connectionState != ConnectionState.done) {
          return const Loading();
        }
        // then, if the snapshot data is null, it means an unknown userId has been passed, so redirect to 404 page
        else if (!snapshot.hasData) {
          context.go('/404');
          return const Loading();
        }
        else {
          MyUser otherUser = snapshot.data as MyUser;

          return Scaffold(
            body: SafeArea(
                child: ChangeNotifierProvider(
                  create: (context) => db,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      //region APP BAR
                      OtherUserAppBar(
                        title: "${otherUser.name ?? otherUser.username}'s Profile",
                        otherUser: otherUser,
                        isAlreadyFavourite: _isAlreadyFavourite,
                        wishlistView: false,
                      ),
                      //endregion

                      Expanded(
                        child: LayoutBuilder(
                          builder: (context, constraints) {
                            if (constraints.maxWidth <= Constants.maxPortraitWidth) {
                              return PortraitView(otherUser: otherUser,);
                            }
                            else {
                              return LandscapeView(otherUser: otherUser,);
                            }
                          },
                        ),
                      ),

                    ],
                  ),
                )
            ),
          );
        }
      }
    );
  }
}

class PortraitView extends StatelessWidget {
  const PortraitView({Key? key, required this.otherUser}) : super(key: key);

  final MyUser otherUser;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //region PROFILE PIC + NAME + USERNAME
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //region PROFILE PIC
                Flexible(
                  flex: 1,
                  child: Consumer<DatabaseService>(
                    builder: (context, db, child) {
                      return InkWell(
                        onTap: () => PhotoView(imageProvider: otherUser.profilePic?.image),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15.0),
                          child: Container(
                            width: 150.0,
                            height: 150.0,
                            color: Theme.of(context).colorScheme.background,
                            child: otherUser.profilePic ?? const RandomColoredBox(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                //endregion
                // const SizedBox(width: 25.0,),

                Flexible(
                  flex: 1,
                  child: Center(
                    child: Column(
                      children: [
                        //region NAME
                        if (otherUser.name != null) Text(
                          otherUser.name!,
                          style: const TextStyle(fontSize: 28.0),
                          textAlign: TextAlign.center,
                        ),
                        //endregion
                        const SizedBox(height: 10.0,),
                        //region USERNAME
                        Text(
                          '@${otherUser.username}',
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.grey[700],
                              fontStyle: FontStyle.italic
                          ),
                        ),
                        //endregion
                      ],
                    ),
                  ),
                )
              ],
            ),
            //endregion
            const SizedBox(height: 25.0,),

            //region BIRTHDAY
            if (otherUser.birthday != null) Row(
              children: [
                Icon(Icons.cake, size: 32.0, color: Theme.of(context).colorScheme.onBackground,),
                const SizedBox(width: 10.0,),
                Padding(
                  padding: const EdgeInsets.only(top: 9.0),
                  child: Text(
                    DateFormat('MMMM, dd').format(otherUser.birthday!).toString(),
                    style: const TextStyle(fontSize: 22.0),
                  ),
                ),
              ],
            ),
            //endregion
            const SizedBox(height: 25.0,),
            //region ABOUT
            if (otherUser.about != null) Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  'About ${otherUser.name ?? otherUser.username}:',
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 5.0,),
                Material(
                  color: Theme.of(context).colorScheme.tertiary,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 8.0,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: TextField(
                      controller: TextEditingController(text: otherUser.about),
                      minLines: 10,
                      maxLines: null,
                      enabled: false,
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(height: 50.0,),
            //endregion

            //region SHARE BUTTON
            Builder(
              builder: (context) {
                // required for the sharePositionOrigin needed to share from iPads
                RenderBox? box;
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  box = context.findRenderObject() as RenderBox?;
                });

                return IconButton(
                  iconSize: 48.0,
                  padding: const EdgeInsets.only(right: 5.0),
                  onPressed: () async {
                    Rect? sharePos;
                    if (box != null) {
                      sharePos = box!.localToGlobal(Offset.zero) & box!.size;
                    }

                    await Share.share(
                      Constants.sharingMessage(otherUser),
                      sharePositionOrigin: sharePos,
                    );
                  },
                  icon: Icon(
                    Icons.share,
                    color: Theme.of(context).colorScheme.onBackground,
                  ),
                );
              }
            ),
            //endregion
          ],
        ),
      ),
    );
  }
}

class LandscapeView extends StatelessWidget {
  const LandscapeView({Key? key, required this.otherUser}) : super(key: key);

  final MyUser otherUser;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        //region COLUMN 1
        Flexible(
          flex: 4,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(50.0),
              child: Column(
                children: [
                  //region PROFILE PIC
                  Padding(
                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.width / 50,),
                    child: Consumer<DatabaseService>(
                      builder: (context, db, child) {
                        return InkWell(
                          onTap: () => PhotoView(imageProvider: otherUser.profilePic?.image),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(15.0),
                            child: Container(
                              width: MediaQuery.of(context).size.height / 4,
                              height: MediaQuery.of(context).size.height / 4,
                              color: Theme.of(context).colorScheme.background,
                              child: otherUser.profilePic ?? const RandomColoredBox(),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  //endregion

                  //region NAME
                  if (otherUser.name != null)
                  Padding(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width / 35,
                      right: MediaQuery.of(context).size.width / 35,
                    ),
                    child: Text(
                      otherUser.name!,
                      style: const TextStyle(fontSize: 28.0),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  //endregion

                  //region USERNAME
                  Padding(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width / 35,
                      right: MediaQuery.of(context).size.width / 35,
                      bottom: MediaQuery.of(context).size.width / 50,
                    ),
                    child: Text(
                      '@${otherUser.username}',
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.grey[700],
                          fontStyle: FontStyle.italic
                      ),
                    ),
                  ),
                  //endregion

                  //region BIRTHDAY
                  if (otherUser.birthday != null) Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width / 35,
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.cake, size: 32.0, color: Theme.of(context).colorScheme.onBackground,),
                        const SizedBox(width: 10.0,),
                        Padding(
                          padding: const EdgeInsets.only(top: 9.0),
                          child: Text(
                            DateFormat('MMMM, dd').format(otherUser.birthday!).toString(),
                            style: const TextStyle(fontSize: 22.0),
                          ),
                        ),
                      ],
                    ),
                  ),
                  //endregion
                ],
              ),
            ),
          )
        ),
        //endregion

        //region SHARE BUTTON
        Flexible(
          flex: 1,
          child: Builder(
            builder: (context) {
              // required for the sharePositionOrigin needed to share from iPads
              RenderBox? box;
              WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                box = context.findRenderObject() as RenderBox?;
              });

              return IconButton(
                iconSize: 48.0,
                padding: const EdgeInsets.only(right: 5.0),
                onPressed: () async {
                  Rect? sharePos;
                  if (box != null) {
                    sharePos = box!.localToGlobal(Offset.zero) & box!.size;
                  }

                  await Share.share(
                    Constants.sharingMessage(otherUser),
                    sharePositionOrigin: sharePos,
                  );
                },
                icon: Icon(
                  Icons.share,
                  color: Theme.of(context).colorScheme.onBackground,
                ),
              );
            }
          ),
        ),
        //endregion

        //region COLUMN 2 (About)
        if (otherUser.about != null) Flexible(
          flex: 3,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(50.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'About ${otherUser.name ?? otherUser.username}:',
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 5.0,),
                  Material(
                    color: Theme.of(context).colorScheme.tertiary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    elevation: 8.0,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: TextField(
                        controller: TextEditingController(text: otherUser.about),
                        minLines: 10,
                        maxLines: null,
                        enabled: false,
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ),
        //endregion
      ],
    );
  }
}
