/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/screens/wishlist/presents_grid.dart';
import 'package:presents/screens/wishlist/presents_list.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/screens/search/other_user_appbar.dart';
import 'package:presents/utils/constants.dart';
import 'package:presents/utils/loading.dart';
import 'package:provider/provider.dart';

/// OtherUser's Wishlist page
class OtherUserWishlist extends StatefulWidget {
  const OtherUserWishlist({Key? key, required this.authService, required this.otherUserUID}) : super(key: key);

  final AuthService authService;
  final String otherUserUID;

  @override
  State<OtherUserWishlist> createState() => _OtherUserWishlistState();
}

class _OtherUserWishlistState extends State<OtherUserWishlist> {

  late bool _isGuest;
  bool _isAlreadyFavourite = false;

  @override
  void initState() {
    super.initState();

    // check if the user is viewing his profile as a guest
    _isGuest = (widget.authService.currUser!.uid != widget.otherUserUID);

    // check if the other user is in the favourites list of the current user
    for (MyUser usr in widget.authService.currUser!.favUsers) {
      if (usr.uid == widget.otherUserUID) {
        _isAlreadyFavourite = true;
        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    AuthService auth = Provider.of<AuthService>(context);
    DatabaseService db = DatabaseService(authService: auth, uid: auth.currUser!.uid);

    return FutureBuilder(
      future: db.getUserData(userId: widget.otherUserUID),
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return const Loading();
        }
        else if (!snapshot.hasData) {
          context.go('/404');
          return const Loading();
        }
        else {
          MyUser otherUser = snapshot.data as MyUser;

          return Scaffold(
            body: SafeArea(
              child: ChangeNotifierProvider(
                create: (context) => db,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    //region APP BAR
                    OtherUserAppBar(
                      title: "${otherUser.name ?? otherUser.username}'s Wishlist",
                      otherUser: otherUser,
                      isAlreadyFavourite: _isAlreadyFavourite,
                      wishlistView: true,
                    ),
                    //endregion

                    //region WISHLIST
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25.0),
                        child: LayoutBuilder(
                          builder: (context, constraints) {
                            if (constraints.maxWidth <= Constants.maxPortraitWidth) {
                              return PresentsList(
                                presentsOwner: otherUser,
                                guestView: _isGuest,
                              );
                            }
                            else {
                              return PresentsGrid(
                                presentsOwner: otherUser,
                                guestView: _isGuest,
                              );
                            }
                          }
                        ),
                      ),
                    ),
                    //endregion

                  ],
                ),
              ),
            ),
          );
        }
      }
    );
  }
}
