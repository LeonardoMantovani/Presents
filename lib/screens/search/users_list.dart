/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/models/my_user.dart';
import 'package:presents/screens/search/user_card.dart';

/// Stateless widget which returns a ListView of UserCards for the given users
class UsersList extends StatelessWidget {
  const UsersList({Key? key, required this.users, required this.emptyText}) : super(key: key);

  final List<MyUser> users;
  final String emptyText;

  @override
  Widget build(BuildContext context) {
    return (users.isEmpty) ? Center(child: Text(emptyText)) : Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: ListView.builder(
          itemCount: users.length,
          itemBuilder: (context, index) {
            if (users[index].uid == 'divider') {
              return const Divider(
                indent: 20.0,
                endIndent: 20.0,
              );
            }
            else {
              return UserCard(user: users[index]);
            }
          }
      ),
    );
  }
}
