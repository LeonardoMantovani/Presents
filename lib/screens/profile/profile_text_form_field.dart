/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';

/// A customized TextFormField to be used in the Profile screen
class ProfileTextFormField extends StatelessWidget {
  // Constructor
  const ProfileTextFormField({
    Key? key,
    required this.controller,
    required this.hintText,
    this.icon,
    this.validator,
    this.onChanged,
  }) : super(key: key);

  final TextEditingController controller;
  final String? Function(String?)? validator;
  final IconData? icon;
  final String hintText;
  final Function(String)? onChanged;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: SizedBox(
        height: 40.0,
        child: TextFormField(
          controller: controller,
          validator: validator,
          decoration: InputDecoration(
            prefixIcon: Icon(icon),
            hintText: hintText,
            contentPadding: const EdgeInsets.only(top: 10.0),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(color: Colors.grey[100]!, width: 0.1),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(
                  color: Theme.of(context).colorScheme.onBackground,
                  width: 2
              ),
            ),
          ),
          onChanged: onChanged,
        ),
      ),
    );
  }
}