/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:date_field/date_field.dart';
import 'package:flutter/material.dart';

/// A customized FormField to pick user's birth date
class ProfileDateTimeFormField extends StatelessWidget {
  // Constructor
  const ProfileDateTimeFormField({
    Key? key,
    required this.hintText,
    required this.icon,
    required this.onDateSelected,
    required this.lastDate,
    this.initialValue,
  }) : super(key: key);

  final IconData icon;
  final String hintText;
  final void Function(DateTime) onDateSelected;
  final DateTime lastDate;
  final DateTime? initialValue;


  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: SizedBox(
        height: 40.0,
        child: DateTimeFormField(
          lastDate: lastDate,
          initialValue: initialValue,
          mode: DateTimeFieldPickerMode.date,
          decoration: InputDecoration(
              prefixIcon: Icon(icon),
              hintText: hintText,
              contentPadding: const EdgeInsets.only(top: 10.0)
          ),
          onDateSelected: onDateSelected,
        )
      ),
    );
  }
}