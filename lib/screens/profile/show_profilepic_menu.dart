/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photo_view/photo_view.dart';
import 'package:presents/services/database.dart';

/// A method to show the profile pic popup menu where the user tapped
Future<void> showProfilePicMenu({
  required BuildContext context, required DatabaseService dbService,
  required Offset offset, required Image? currProfilePic,}) async {

  final screenSize = MediaQuery.of(context).size;
  final ImagePicker picker = ImagePicker();

  // create the option list with only 'upload' as an option
  List<PopupMenuEntry<String>> options = <PopupMenuEntry<String>>[
    const PopupMenuItem<String>(value: 'upload', child: Text('Upload a picture'))
  ];

  // add the 'view' and 'delete' options only if there is already a profile pic
  if (currProfilePic != null) {
    options.insert(0, const PopupMenuItem<String>(value: 'view', child: Text('View your profile picture')));
    options.insert(2, const PopupMenuItem<String>(value: 'remove', child: Text('Remove your profile picture')));
  }

  // return the popup menu where the user tapped
  await showMenu(
    context: context,
    position: RelativeRect.fromLTRB(
      offset.dx,
      offset.dy,
      screenSize.width - offset.dx,
      screenSize.height - offset.dy,
    ),
    items: options,
    elevation: 8.0,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
  ).then((value) async {
    switch (value) {
      case 'view':
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => Material(
            child: SafeArea(
              child: Stack(
                children: [
                  PhotoView(imageProvider: currProfilePic!.image),
                  Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 25.0),
                      child: IconButton(
                        onPressed: () => Navigator.of(context).pop(),
                        icon: const Icon(Icons.close, size: 48, color: Colors.grey,)),
                    ),
                    ),
                ],
              ),
            ),
          ))
        );
        break;
      case 'upload':
        XFile? image = await picker.pickImage(source: ImageSource.gallery);
        if (image != null) {
          await dbService.uploadProfilePic(image);
        }
        break;
      case 'remove':
        await dbService.removeProfilePic();
        break;
    }
  });
}