/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/services/database.dart';
import 'package:presents/utils/constants.dart';
import 'package:presents/utils/default_appbar.dart';
import 'package:presents/utils/custom_button_style.dart';
import 'package:presents/utils/default_text_form_field.dart';
import 'package:presents/utils/default_snackbar.dart';
import 'package:presents/utils/loading.dart';
import 'package:presents/utils/routing_utils.dart';
import 'package:provider/provider.dart';


/// SignUp Screen
class SignUp extends StatelessWidget {
  SignUp({Key? key, String? passedNextDest}) :
    nextDest = passedNextDest?.replaceAll('__', '/'),
    super(key: key);

  final String? nextDest;

  @override
  Widget build(BuildContext context) {

    void navigateAfterSignup() {
      if (nextDest == null) {
        context.go('/');
      }
      else {
        context.go(nextDest!);
      }
    }

    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth <= Constants.maxPortraitWidth) {
          return PortraitSignUp(
            navigateAfterSignup: navigateAfterSignup,
            nextDest: nextDest,
          );
        }
        else {
          return LandscapeSignUp(
            navigateAfterSignup: navigateAfterSignup,
            nextDest: nextDest,
          );
        }
      }
    );
  }
}


/// Portrait Layout for the SignUp Screen
class PortraitSignUp extends StatefulWidget {
  const PortraitSignUp({
    Key? key,
    required this.navigateAfterSignup,
    required this.nextDest
  }) : super(key: key);

  final Function navigateAfterSignup;
  final String? nextDest;

  @override
  State<PortraitSignUp> createState() => _PortraitSignUpState();
}
class _PortraitSignUpState extends State<PortraitSignUp> {
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthService>(
        builder: (context, auth, child) {
          // create a temporary instance of the DatabaseService to get existing usernames
          DatabaseService tmpDB = DatabaseService(authService: auth, uid: '');

          return FutureBuilder(
              future: tmpDB.getAllUsernames(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Loading();
                }
                else {
                  return _loading ? const Loading() : Scaffold(
                    body: SafeArea(
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            //region APP BAR
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: DefaultAppBar(
                                title: 'Sign up',
                                actions: <Widget>[
                                  TextButton.icon(
                                    icon: const Icon(Icons.person, size: 28.0,),
                                    label: const Text('Log In', style: TextStyle(fontSize: 18.0),),
                                    onPressed: () {
                                      String? next = widget.nextDest?.substring(1);
                                      if (next == null || next == '') {
                                        context.goNamed(routeName(RouteCodes.login),);
                                      }
                                      else {
                                        context.goNamed(routeName(RouteCodes.login), queryParams: {'next': next});
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                            //endregion

                            //region FORM
                            Expanded(
                              child: SignUpForm(
                                usernamesSnap: snapshot,
                                auth: auth,
                                setLoading: (bool b) => setState(() => _loading = b),
                                showError: (String e) => ScaffoldMessenger.of(context).showSnackBar(defaultSnackBar(e)),
                                navigateAfterSignup: widget.navigateAfterSignup,
                              ),
                            ),
                            //endregion
                          ],),
                      ),
                    ),
                  );
                }
              }
          );
        }
    );
  }
}


/// Landscape Layout for the SignUp Screen
class LandscapeSignUp extends StatefulWidget {
  const LandscapeSignUp({
    Key? key,
    required this.navigateAfterSignup,
    required this.nextDest,
  }) : super(key: key);

  final Function navigateAfterSignup;
  final String? nextDest;

  @override
  State<LandscapeSignUp> createState() => _LandscapeSignUpState();
}
class _LandscapeSignUpState extends State<LandscapeSignUp> {
  bool _loading = false;
  @override
  Widget build(BuildContext context) {
    return Consumer<AuthService>(
        builder: (context, auth, child) {
          // create a temporary instance of the DatabaseService to get existing usernames
          DatabaseService tmpDB = DatabaseService(authService: auth, uid: '');

          return FutureBuilder(
              future: tmpDB.getAllUsernames(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Loading();
                }
                else {
                  return _loading ? const Loading() : Scaffold(
                    body: SafeArea(
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            //region APP BAR
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: DefaultAppBar(
                                title: 'Sign up',
                                actions: <Widget>[
                                  TextButton.icon(
                                    icon: const Icon(Icons.person, size: 28.0,),
                                    label: const Text('Log In', style: TextStyle(fontSize: 18.0),),
                                    onPressed: () {
                                      String next = widget.nextDest?.substring(1) ?? '';
                                      if (next == '') {
                                        context.goNamed(routeName(RouteCodes.login),);
                                      }
                                      else {
                                        context.goNamed(routeName(RouteCodes.login), queryParams: {'next': next});
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                            //endregion

                            Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  //region LOGO
                                  Flexible(flex: 2,
                                    child: Builder(
                                      builder: (context) {
                                        if (Theme.of(context).brightness == Brightness.light) {
                                          return Image.asset('assets/images/auth-logo-b.png');
                                        }
                                        else {
                                          return Image.asset('assets/images/auth-logo-w.png');
                                        }
                                      }
                                    ),
                                  ),
                                  //endregion

                                  //region FORM
                                  Flexible(
                                    flex: 3,
                                    child: SignUpForm(
                                      usernamesSnap: snapshot,
                                      auth: auth,
                                      setLoading: (bool b) => setState(() => _loading = b),
                                      showError: (String e) => ScaffoldMessenger.of(context).showSnackBar(defaultSnackBar(e)),
                                      navigateAfterSignup: widget.navigateAfterSignup,
                                    ),
                                  ),
                                  //endregion
                                ],
                              ),
                            ),
                          ],),
                      ),
                    ),
                  );
                }
              }
          );
        }
    );
  }
}


/// SignUp Form (that is common for both Layouts)
class SignUpForm extends StatelessWidget {
  SignUpForm({
    Key? key,
    required this.usernamesSnap,
    required this.setLoading,
    required this.showError,
    required this.auth,
    required this.navigateAfterSignup,
  }) : super(key: key);

  final AsyncSnapshot usernamesSnap;
  final Function setLoading;
  final Function showError;
  final AuthService auth;
  final Function navigateAfterSignup;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _psw = TextEditingController();
  final TextEditingController _repeatPsw = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50.0),
          child: Center( //NOTE: wrapping with center because mainAxisAlignment doesn't work for columns inside SingleChildScrollView
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(height: 10.0,),
                  //region EMAIL FIELD
                  DefaultTextFormField(
                    controller: _email,
                    icon: Icons.alternate_email,
                    label: 'Email',
                    validator: (value) {
                      if (value == null || !value.contains('@') || value.contains(' ')) {
                        return 'Please enter a valid email address';
                      }
                      else {
                        return null;
                      }
                    },
                  ),
                  const SizedBox(height: 30.0,),
                  //endregion

                  //region USERNAME FIELD
                  DefaultTextFormField(
                      controller: _username,
                      icon: Icons.assignment_ind,
                      label: 'Username',
                      validator: (value) {
                        List<String> existingUsernames = usernamesSnap.data as List<String>;
                        if (value == null || value.length < 3) {
                          return 'Username must be 3+ characters long';
                        }
                        else if (existingUsernames.contains(value)) {
                          return 'Username already taken';
                        }
                        else {
                          return null;
                        }
                      }
                  ),
                  const SizedBox(height: 30.0,),
                  //endregion

                  //region PASSWORD FIELD
                  DefaultTextFormField(
                      controller: _psw,
                      icon: Icons.password,
                      label: 'Password',
                      validator: (value) {
                        if (value == null ||
                            value.length < 8) {
                          return 'Passwords must be 8+ characters long';
                        }
                        else {
                          return null;
                        }
                      }
                  ),
                  const SizedBox(height: 30.0,),
                  //endregion

                  //region REPEAT PASSWORD FIELD
                  DefaultTextFormField(
                      controller: _repeatPsw,
                      icon: Icons.password,
                      label: 'Repeat Password',
                      validator: (value) {
                        if (value != _psw.text) {
                          return "Passwords don't match";
                        }
                        else {
                          return null;
                        }
                      }
                  ),
                  const SizedBox(height: 30.0,),
                  //endregion

                  //region SIGNUP BUTTON
                  ElevatedButton(
                    style: customButtonStyle(),
                    child: Text('SIGN UP', style: TextStyle(color: Colors.grey[850])),
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        setLoading(true);
                        String? res = await auth.signUp(
                          email: _email.text,
                          username: _username.text,
                          password: _psw.text,
                        );
                        if (res != null) {
                          showError(res);
                        }
                        setLoading(false);
                        navigateAfterSignup();
                      }
                    },
                  ),
                  //endregion
                ],),
            ),
          ),
        )
    );
  }
}