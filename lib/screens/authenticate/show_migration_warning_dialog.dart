/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> showMigrationWarningDialog(BuildContext context, SharedPreferences shPrefs) async {

  ColorScheme themeColors = Theme.of(context).colorScheme;

  await showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: const Text('Are You coming from Presents V1?'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            "If you have already registered an account on an older version of "
            "Presents (version < 2.0.0), you need to migrate your account before "
            "being able to login.\n"
            "To do so, please visit our Presents Migrator tool using the link below:",
          ),
          const SizedBox(height: 20.0,),
          TextButton.icon(
            onPressed: () => launchUrl(
              Uri.parse('https://migrator.presents.lezsoft.com'),
              mode: LaunchMode.externalApplication,
            ),
            icon: Icon(Icons.arrow_forward, color: themeColors.onBackground,),
            label: Text(
              'Take me to the Presents Migrator',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: themeColors.onBackground,
              ),
            ),
          ),
        ],
      ),
      actionsPadding: const EdgeInsets.only(right: 15.0, bottom: 10.0),
      actions: [
        TextButton(
          onPressed: () {
            shPrefs.setBool('migrationWarningDismissed', true);
            context.pop();
          },
          child: Text(
            "GOT IT. DON'T SHOW THIS AGAIN",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: themeColors.onBackground,
            ),
          ),
        ),
      ],
    )
  );
}