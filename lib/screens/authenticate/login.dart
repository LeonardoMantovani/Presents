/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:presents/screens/authenticate/show_migration_warning_dialog.dart';
import 'package:presents/screens/authenticate/show_reset_password_dialog.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/utils/constants.dart';
import 'package:presents/utils/default_appbar.dart';
import 'package:presents/utils/default_text_form_field.dart';
import 'package:presents/utils/default_snackbar.dart';
import 'package:presents/utils/loading.dart';
import 'package:presents/utils/routing_utils.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Login Screen
class LogIn extends StatefulWidget {
  LogIn({Key? key, String? passedNextDest}) :
    nextDest = passedNextDest?.replaceAll('__', '/'),
    super(key: key);
  
  final String? nextDest;

  @override
  State<LogIn> createState() => _LogInState();
}
class _LogInState extends State<LogIn> {

  bool _migrationWarningShown = false;

  @override
  Widget build(BuildContext context) {
    
    void navigateAfterLogin() {
      if (widget.nextDest == null) {
        context.go('/');
      }
      else {
        context.go(widget.nextDest!);
      }
    }

    return LayoutBuilder(
      builder: (context, constraints) {
        // check in shared_preferences if the user already dismissed the migration warning...
        SharedPreferences.getInstance().then((SharedPreferences shPrefs) {
          // if he didn't (and the warning hasn't been shown yet) show it
          if (shPrefs.getBool('migrationWarningDismissed') != true && !_migrationWarningShown) {
            Future.delayed(const Duration(milliseconds: 500), () => showMigrationWarningDialog(context, shPrefs));
            _migrationWarningShown = true;
          }
        });

        if (constraints.maxWidth <= Constants.maxPortraitWidth) {
          return PortraitLogIn(
            navigateAfterLogin: navigateAfterLogin,
            nextDest: widget.nextDest,
          );
        }
        else {
          return LandscapeLogIn(
            navigateAfterLogin: navigateAfterLogin,
            nextDest: widget.nextDest,
          );
        }
      }
    );
  }
}


/// Portrait Layout for the Login Screen
class PortraitLogIn extends StatefulWidget {
  const PortraitLogIn({
    Key? key,
    required this.navigateAfterLogin,
    required this.nextDest,
  }) : super(key: key);
  
  final Function navigateAfterLogin;
  final String? nextDest;

  @override
  State<PortraitLogIn> createState() => _PortraitLogInState();
}
class _PortraitLogInState extends State<PortraitLogIn> {
  bool _loading = false;
  
  @override
  Widget build(BuildContext context) {
    return Consumer<AuthService>(
        builder: (context, auth, child) {
          return _loading ? const Loading() : Scaffold(
            body: SafeArea(
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    //region APP BAR
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: DefaultAppBar(
                        title: 'Log in',
                        actions: <Widget>[
                          TextButton.icon(
                            icon: const Icon(Icons.person_add, size: 28.0,),
                            label: const Text('Sign Up', style: TextStyle(fontSize: 18.0),),
                            onPressed: () {
                              String next = widget.nextDest?.substring(1) ?? '';
                              if (next == '') {
                                context.goNamed(routeName(RouteCodes.signup),);
                              }
                              else {
                                context.goNamed(
                                  routeName(RouteCodes.signup),
                                  queryParams: {'next': next}
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                    //endregion

                    //region FORM
                    Expanded(
                      child: LogInForm(
                        setLoading: (bool b, {String? errorMessage}) {
                          setState(() => _loading = b);
                          if (errorMessage != null) {
                            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                              ScaffoldMessenger.of(context).showSnackBar(defaultSnackBar(errorMessage));
                            });
                          }
                        },
                        auth: auth,
                        navigateAfterLogin: widget.navigateAfterLogin,
                      ),
                    ),
                    //endregion
                  ],),
              ),
            ),
          );
        }
    );
  }
}


/// Landscape Layout for the LogIn Screen
class LandscapeLogIn extends StatefulWidget {
  const LandscapeLogIn({
    Key? key,
    required this.navigateAfterLogin,
    required this.nextDest,
  }) : super(key: key);
  
  final Function navigateAfterLogin;
  final String? nextDest;

  @override
  State<LandscapeLogIn> createState() => _LandscapeLogInState();
}
class _LandscapeLogInState extends State<LandscapeLogIn> {
  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthService>(
        builder: (context, auth, child) {
          return _loading ? const Loading() : Scaffold(
            body: SafeArea(
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    //region APP BAR
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: DefaultAppBar(
                        title: 'Log In',
                        actions: <Widget>[
                          TextButton.icon(
                            icon: const Icon(Icons.person_add, size: 28.0,),
                            label: const Text('Sign Up', style: TextStyle(fontSize: 18.0),),
                            onPressed: () {
                              String next = widget.nextDest?.substring(1) ?? '';
                              if (next == '') {
                                context.goNamed(routeName(RouteCodes.signup),);
                              }
                              else {
                                context.goNamed(
                                    routeName(RouteCodes.signup),
                                    queryParams: {'next': next}
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                    //endregion

                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          //region LOGO
                          Flexible(flex: 2,
                            child: Builder(
                              builder: (context) {
                                if (Theme.of(context).brightness == Brightness.light) {
                                  return Image.asset('assets/images/auth-logo-b.png');
                                }
                                else {
                                  return Image.asset('assets/images/auth-logo-w.png');
                                }
                              }
                            ),
                          ),
                          //endregion

                          //region FORM
                          Flexible(
                            flex: 3,
                            child: LogInForm(
                              setLoading: (bool b, {String? errorMessage}) {
                                setState(() => _loading = b);
                                if (errorMessage != null) {
                                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                                    ScaffoldMessenger.of(context).showSnackBar(defaultSnackBar(errorMessage));
                                  });
                                }
                              },
                              auth: auth,
                              navigateAfterLogin: widget.navigateAfterLogin,
                            ),
                          ),
                          //endregion
                        ],
                      ),
                    ),
                  ],),
              ),
            ),
          );
        }
    );
  }
}

/// LogIn Form (that is common for both Layouts)
class LogInForm extends StatelessWidget {
  LogInForm({
    Key? key, 
    required this.setLoading, 
    required this.auth,
    required this.navigateAfterLogin,
  }) : super(key: key);

  final Function setLoading;
  final AuthService auth;
  final Function navigateAfterLogin;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _psw = TextEditingController();

  @override
  Widget build(BuildContext context) {

    ColorScheme themeColors = Theme.of(context).colorScheme;

    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25.0),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                //region EMAIL FIELD
                DefaultTextFormField(
                  controller: _email,
                  icon: Icons.alternate_email,
                  label: 'Email',
                  validator: (value) {
                    if (value == null || !value.contains('@') || value.contains(' ')) {
                      return 'Please enter a valid email address';
                    }
                    else {
                      return null;
                    }
                  },
                ),
                const SizedBox(height: 30.0,),
                //endregion

                //region PASSWORD FIELD
                DefaultTextFormField(
                    controller: _psw,
                    icon: Icons.password,
                    label: 'Password',
                    validator: (value) {
                      if (value == null || value.length < 8) {
                        return 'Passwords must be 8+ characters long';
                      }
                      else {
                        return null;
                      }
                    }
                ),
                const SizedBox(height: 15.0,),
                //endregion
                
                //region FORGOT PASSWORD BUTTON
                TextButton(
                  onPressed: () async {
                    await showResetPasswordDialog(context, defaultEmail: _email.text);
                  },
                  child: Text(
                    'Forgot your password?', 
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      color: themeColors.onBackground,
                    ),
                  ),
                ),
                const SizedBox(height: 30.0,),
                //endregion

                //region LOGIN BUTTON
                ElevatedButton(
                  // style: customButtonStyle(),
                  child: const Text('LOG IN'),
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      setLoading(true);
                      String? res = await auth.logIn(
                        email: _email.text,
                        password: _psw.text,
                      );
                      if (res != null) {
                        setLoading(false, errorMessage: res);
                      }
                      else {
                        navigateAfterLogin();
                      }
                      // setLoading(false);
                    }
                  },
                ),
                //endregion
              ],),
          ),
        ),
      )
    );
  }
}
