/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/// Model for presents that can be both created through the traditional constructor
/// or converted from JSON (as they are stored in the database)
class Present {
  // Traditional Constructor
  Present({
    required this.id,
    required this.title,
    required this.price,
    required this.priority,
    this.link,
    this.donorUid,
  });

  // // Constructor to create a new Present from a JSON string map
  // Present.fromJson(Map<String, dynamic> json) :
  //   id = json['id'],
  //   title = json['title'],
  //   price = json['price'].toDouble(),
  //   priority = json['priority'],
  //   link = json['link'],
  //   donorUid = json['donorUid'];

  final String id;
  String title;
  double price;
  int priority;
  String? link;
  String? donorUid;

  // Method to convert a Present object into a jason string map
  Map<String, dynamic> toJson() => {
    'title': title,
    'price': price,
    'priority': priority,
    'link': link,
    'donorUid': donorUid
  };
}