/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:presents/utils/routing_utils.dart';

/// Customized BottomNavigationBar Widget
Widget orizzNavBar(BuildContext context, int tab) {

  ColorScheme themeColors = Theme.of(context).colorScheme;

  Map<String, Color?> backColors = <String, Color?>{
    'wishlist': ((tab == 0) ? themeColors.primary : null),
    'search': ((tab == 1) ? themeColors.primary : null),
    'profile': ((tab == 2) ? themeColors.primary : null),
  };

  Map<String, Color?> iconColors = <String, Color?>{
    'wishlist': ((tab == 0) ? themeColors.onPrimary : themeColors.onTertiary),
    'search': ((tab == 1) ? themeColors.onPrimary : themeColors.onTertiary),
    'profile': ((tab == 2) ? themeColors.onPrimary : themeColors.onTertiary),
  };

  return SizedBox(
    height: 75.0,
    child: Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      color: themeColors.tertiary,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Container(
              height: 75.0,
              width: 110.0,
              color: backColors['wishlist'],
              child: IconButton(
                onPressed: () {
                  context.goNamed(routeName(RouteCodes.wishlist));
                },
                icon: Icon(
                  Icons.format_list_bulleted,
                  size: 32.0,
                  color: iconColors['wishlist'],
                ),
              ),
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Container(
              color: backColors['search'],
              height: 75.0,
              width: 110.0,
              child: IconButton(
                onPressed: () {
                  context.goNamed(routeName(RouteCodes.search));
                },
                icon: Icon(
                  Icons.search,
                  size: 32.0,
                  color: iconColors['search'],
                ),
              ),
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Container(
              color: backColors['profile'],
              height: 75.0,
              width: 110.0,
              // color: Theme.of(context).colorScheme.primary,
              child: IconButton(
                onPressed: () {
                  context.goNamed(routeName(RouteCodes.profile));
                },
                icon: Icon(
                  Icons.account_circle,
                  size: 32.0,
                  color: iconColors['profile'],
                ),
              ),
            ),
          ),
        ],),
    ),
  );
}