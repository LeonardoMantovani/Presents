/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'dart:math';

import 'package:flutter/material.dart';

/// method which returns a random color between Flutter primaries ones
Color _getRandomColor() {
  return Colors.primaries[Random().nextInt(Colors.primaries.length)];
}

/// A Random colored container to be used when images are not available
class RandomColoredBox extends StatelessWidget {
  const RandomColoredBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(color: _getRandomColor(),);
  }
}