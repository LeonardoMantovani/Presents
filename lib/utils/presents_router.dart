/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:go_router/go_router.dart';
import 'package:presents/screens/page_not_found.dart';
import 'package:presents/screens/auth_initializer.dart';
import 'package:presents/screens/authenticate/login.dart';
import 'package:presents/screens/authenticate/password_reset.dart';
import 'package:presents/screens/authenticate/signup.dart';
import 'package:presents/screens/home.dart';
import 'package:presents/screens/search/other_user_profile.dart';
import 'package:presents/screens/search/other_user_wishlist.dart';
import 'package:presents/services/auth.dart';
import 'package:presents/utils/routing_utils.dart';

class PresentsRouter {
  PresentsRouter({required this.authService});
  
  final AuthService authService;

  late final GoRouter router = GoRouter(
    refreshListenable: authService,
    routes: <GoRoute>[
      //region HOME ROUTES
      GoRoute(
        name: routeName(RouteCodes.wishlist),
        path: routePath(RouteCodes.wishlist),
        pageBuilder: (context, state) => NoTransitionPage(
          key: state.pageKey,
          child: const Home(index: 0),
        ),
      ),
      GoRoute(
        name: routeName(RouteCodes.search),
        path: routePath(RouteCodes.search),
        pageBuilder: (context, state) => NoTransitionPage(
          key: state.pageKey,
          child: const Home(index: 1)
        ),
        routes: <GoRoute>[
          //region OTHER USERS VIEWS ROUTES
          GoRoute(
            name: routeName(RouteCodes.otherWishlist),
            path: routePath(RouteCodes.otherWishlist),
            builder: (context, state) => OtherUserWishlist(
              authService: authService,
              otherUserUID: state.params['uid']!
            ),
          ),
          GoRoute(
            name: routeName(RouteCodes.otherProfile),
            path: routePath(RouteCodes.otherProfile),
            builder: (context, state) => OtherUserProfile(
              authService: authService,
              otherUserUID: state.params['uid']!
            ),
          ),
          //endregion
        ]
      ),
      GoRoute(
        name: routeName(RouteCodes.profile),
        path: routePath(RouteCodes.profile),
        pageBuilder: (context, state) => NoTransitionPage(
          key: state.pageKey,
          child: const Home(index: 2,)
        ),
      ),
      //endregion

      //region AUTH ROUTES
      GoRoute(
        name: routeName(RouteCodes.login),
        path: routePath(RouteCodes.login),
        pageBuilder: (context, state) {
          String? nextDest;
          if (state.queryParams['next'] != null) {
            nextDest = '/${state.queryParams['next']}';
          }
          return NoTransitionPage(
            key: state.pageKey,
            child: LogIn(passedNextDest: nextDest,),
          );
        },
      ),
      GoRoute(
        name: routeName(RouteCodes.signup),
        path: routePath(RouteCodes.signup),
        pageBuilder: (context, state) {
          String? nextDest;
          if (state.queryParams['next'] != null) {
            nextDest = '/${state.queryParams['next']}';
          }
          return NoTransitionPage(
            key: state.pageKey,
            child: SignUp(passedNextDest: nextDest,),
        );
        },
      ),
      GoRoute(
        name: routeName(RouteCodes.pswReset),
        path: routePath(RouteCodes.pswReset),
        pageBuilder: (context, state) => NoTransitionPage(
          key: state.pageKey,
          child: PasswordReset(
            userId: state.queryParams['userId'],
            secret: state.queryParams['secret'],
          ),
        ),
      ),

      GoRoute(
        name: routeName(RouteCodes.authInit),
        path: routePath(RouteCodes.authInit),
        pageBuilder: (context, state) {
          String originalDest = '/${state.params['next']!}';

          // Add the query  parameters (if any) to the originalDest string
          List<MapEntry<String, String>> qParams = state.queryParams.entries.toList();
          if (qParams.isNotEmpty) {
            originalDest = '$originalDest?${qParams[0].key}=${qParams[0].value}';
            for (int i=1; i < qParams.length; i++) {
              originalDest = '$originalDest&${qParams[i].key}=${qParams[i].value}';
            }
          }

          if (originalDest == '/wishlist') {
            originalDest = '/';
          }

          return NoTransitionPage(
            key: state.pageKey,
            child: AuthServiceInitializer(passedOriginalDest: originalDest)
          );
        },
      ),
      //endregion
    ],
    errorPageBuilder: (context, state) => const NoTransitionPage(child: PageNotFound()),
    redirect: (context, state) async {
      RouteCodes? routeCode = getCodeFromPath(state.subloc);
      // Proceed with redirecting only if the route is a known one
      if (routeCode != null) {
        // Check if the AuthService has already been initialized
        if (authService.initialized == false &&
          !state.subloc.contains('/loading')) {
          // Store the current state "target" in a variable
          String next = state.location.substring(1);
          if (next == '') {
            next = 'wishlist';
          }

          // replace all '/' in the next var with '__' so that it acts as a unique parameter
          next = next.replaceAll('/', '__');

          // Redirect to the AuthInit screen that keeps track of the next "target"
          return "${routePath(RouteCodes.authInit, forRouteDeclaration: false)}/$next";
        }
        else if (authService.initialized == true) {
          // Check if the user is authenticated
          bool authenticated = authService.currUser != null;

          // If the user is trying to access the LogIn or SignUp pages...
          if (routeCode == RouteCodes.login ||
              routeCode == RouteCodes.signup) {
            // ... redirect to Wishlist if authenticated
            return authenticated
              ? routePath(RouteCodes.wishlist, forRouteDeclaration: false)
              : null;
          }
          // Else if the user is trying to access the password-reset page just show it
          else if (routeCode == RouteCodes.pswReset) {
            return null;
          }
          // Else (a.k.a. for all other pages)...
          else {
            if (authenticated) {
              return null;
            }
            else {
              // Store the current state "target" in a variable
              String next = state.subloc.substring(1);
              // and redirect to login
              if (next == '') {
                return routePath(RouteCodes.login, forRouteDeclaration: false);
              }
              else {
                // replace all '/' in the next var with '__' so that it acts as a unique parameter
                next = next.replaceAll('/', '__');

                return "${routePath(RouteCodes.login, forRouteDeclaration: false)}?next=$next";
              }
            }
          }
        }
      }
      // if arrived here, an unknown route has been requested, so return null
      // (and the errorPageBuilder will show the 404 page)
      return null;
    }
  );
}