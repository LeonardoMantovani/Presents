/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:presents/models/my_user.dart';

class Constants {
  /// AppWrite's Project ID
  static const String awProjectID = 'presents';
  /// AppWrite Server's Endpoint
  static const String awEndpoint = 'https://server.lezsoft.com/v1';

  /// AppWrite Databases' IDs
  static const String defaultDbID = 'default';
  static const String wishlistsDbID = 'wishlists_db';

  /// AppWrite collections' IDs
  static const String usersDataCollID = 'userData';

  /// AppWrite buckets' IDs
  static const String profilePicsBucketID = 'profilePics';


  /// Landscape Mode minimum width
  static const int maxPortraitWidth = 1100;

  /// Profile sharing message
  static String sharingMessage(MyUser user, {bool currUser = false}) {
    if (currUser) {
      return 'Hey, I\'m using Presents!\n'
          'Check out my profile at https://presents.lezsoft.com/search/profile/${user.uid}\n\n'
          'Don\'t know what Presents is? Learn more about it at lezsoft.com/presents';
    }
    else {
      return 'Hey, ${user.name ?? user.username} is using Presents!\n'
          'Check out his profile at https://presents.lezsoft.com/search/profile/${user.uid}\n\n'
          'Don\'t know what Presents is? Learn more about it at lezsoft.com/presents';
    }
  }
}