/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';

/// Customized AppBar to be used as default
class DefaultAppBar extends StatelessWidget {
  //Constructor
  DefaultAppBar({Key? key, required this.title, List<Widget>? actions}) : super(key: key) {
    actions?.add(const SizedBox(width: 8.0,));
    _actions = actions;
  }

  final String title;
  late final List<Widget>? _actions;

  @override
  Widget build(BuildContext context) {

    ColorScheme themeColors = Theme.of(context).colorScheme;

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: AppBar(
        backgroundColor: themeColors.primary,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        leading: Image.asset('assets/images/icon.png'),
        titleSpacing: 5.0,
        title: FittedBox(fit: BoxFit.fitWidth, child: Text(title)),
        actions: _actions,
      ),
    );
  }
}