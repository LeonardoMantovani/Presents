/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';
import 'package:presents/services/theme.dart';
import 'package:presents/utils/triple_switch_states.dart';

/// Triple-state switch for application theme selection
class ThemeSwitcher extends StatefulWidget {
  const ThemeSwitcher({
    Key? key, required this.iconLeft, required this.iconMiddle,
    required this.iconRight, required this.themeManager, required this.updateNightModeText,
  }) : super(key: key);
  
  final IconData iconLeft;
  final IconData iconMiddle;
  final IconData iconRight;
  final ThemeManager themeManager;
  final Function updateNightModeText;

  @override
  State<ThemeSwitcher> createState() => _ThemeSwitcherState();
}

class _ThemeSwitcherState extends State<ThemeSwitcher> {

  late SwitchState selection;

  /// Function to update the current ThemeMode in the Theme Manager
  void _updateThemeManager(SwitchState newState) {
    late ThemeMode newThemeMode;
    switch (newState) {
      case SwitchState.left:
        newThemeMode = ThemeMode.light;
        break;
      case SwitchState.middle:
        newThemeMode = ThemeMode.system;
        break;
      case SwitchState.right:
        newThemeMode = ThemeMode.dark;
        break;
    }
    widget.themeManager.updateCurrTheme(newThemeMode: newThemeMode);
    widget.updateNightModeText(newThemeMode);
  }

  /// Function that returns the proper "content" for the switch based on current selection
  Widget _getContent(ColorScheme themeColors) {

    late double thumbPadding;
    late double iconPadding;
    late Icon icon;

    switch (selection) {
      case SwitchState.left:
        thumbPadding = 0;
        iconPadding = 55;
        icon = Icon(widget.iconLeft, color: themeColors.onTertiary,);
        break;
      case SwitchState.middle:
        thumbPadding = 27.5;
        iconPadding = 35.5;
        icon = Icon(widget.iconMiddle, color: themeColors.background,);
        break;
      case SwitchState.right:
        thumbPadding = 50;
        iconPadding = 10;
        icon = Icon(widget.iconRight, color: themeColors.onTertiary,);
        break;
    }

    return Stack(
      children: [
        //region Thumb
        Padding(
          padding: EdgeInsets.only(left: thumbPadding),
          child: GestureDetector(
            onHorizontalDragEnd: (details) {
              // if the user swiped right...
              if (details.velocity.pixelsPerSecond.dx > 0) {
                SwitchState newSelection = nextSwitchState(selection);
                _updateThemeManager(newSelection);
                setState(() => selection = newSelection);
              }
              // else if the user swiped left...
              else if (details.velocity.pixelsPerSecond.dx < 0) {
                SwitchState newSelection = prevSwitchState(selection);
                _updateThemeManager(newSelection);
                setState(() => selection = newSelection);
              }
            },
            onTapUp: (details) {
              SwitchState newSelection = loopSwitchState(selection);
              _updateThemeManager(newSelection);
              setState(() => selection = newSelection);
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(25.0),
              child: Container(
                width: 40,
                height: 45,
                color: themeColors.onTertiary,
              ),
            ),
          ),
        ),
        //endregion
        //region Icon
        Padding(
          padding: EdgeInsets.only(left: iconPadding),
          child: Center(child: IgnorePointer(child: icon)),
        ),
        //endregion
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    late int initialValue;
    switch (widget.themeManager.currThemeMode) {
      case ThemeMode.light:
        initialValue = 0;
        break;
      case ThemeMode.system:
        initialValue = 1;
        break;
      case ThemeMode.dark:
        initialValue = 2;
        break;
      default:
        initialValue = 1; // if currThemeMode is null set it as system
        break;
    }
    selection = SwitchState.values[initialValue];
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50.0,
      width: 100.0,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        elevation: 10.0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _getContent(Theme.of(context).colorScheme),
          ],
        ),
      ),
    );
  }
}
