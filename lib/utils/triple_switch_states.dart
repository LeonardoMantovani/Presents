/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/// Support enumeration with the three possible states of a triple-switch Widget
enum SwitchState {
  left,
  middle,
  right
}

/// Loop through the enumeration.
/// returns the next SwitchState (or the first one if the enumeration ends)
SwitchState loopSwitchState(SwitchState currSwitchState) {
  int nextIndex = (currSwitchState.index + 1) % SwitchState.values.length;
  return SwitchState.values[nextIndex];
}

/// Returns the next SwitchState (or the passed one if the enumeration ends)
SwitchState nextSwitchState(SwitchState currSwitchState) {
  int nextIndex = currSwitchState.index + 1;
  if (nextIndex >= SwitchState.values.length) {
    return currSwitchState;
  }
  else {
    return SwitchState.values[nextIndex];
  }
}

/// Returns the previous SwitchState (or the passed one if the enumeration ends)
SwitchState prevSwitchState(SwitchState currSwitchState) {
  int prevIndex = currSwitchState.index - 1;
  if (prevIndex < 0) {
    return currSwitchState;
  }
  else {
    return SwitchState.values[prevIndex];
  }
}