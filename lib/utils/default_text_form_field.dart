/*
 * This file is part of Presents.
 * Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
 * you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
 * Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
 *
 * SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import 'package:flutter/material.dart';

/// Customized TextFormField to be used as default
class DefaultTextFormField extends StatefulWidget {
  //Constructor
  DefaultTextFormField({
    Key? key,
    required this.controller,
    required this.icon,
    required this.label,
    required this.validator,
    this.keyboardType,
  }) : super(key: key) {
    isPassword = label.toLowerCase().contains("password");
  }

  final TextEditingController controller;
  final IconData icon;
  final String label;
  final String? Function(String?) validator;
  final TextInputType? keyboardType;
  late final bool isPassword;

  @override
  State<DefaultTextFormField> createState() => _DefaultTextFormFieldState();
}

class _DefaultTextFormFieldState extends State<DefaultTextFormField> {

  bool _showPassword = false;

  @override
  Widget build(BuildContext context) {

    ColorScheme themeColors = Theme.of(context).colorScheme;

    return Material(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: TextFormField(
        controller: widget.controller,
        validator: widget.validator,
        obscureText: widget.isPassword && !_showPassword,
        decoration: InputDecoration(
          labelText: widget.label,
          prefixIcon: Icon(widget.icon),
          suffixIcon: widget.isPassword ? IconButton(
            icon: Icon(
              _showPassword ? Icons.visibility_off : Icons.visibility,
              color: themeColors.onBackground,
            ),
            padding: const EdgeInsets.only(right: 8.0),
            onPressed: () {
              setState(() => _showPassword = !_showPassword);
            },
          ) : null,
        ),
        keyboardType: widget.keyboardType,
      ),
    );
  }
}