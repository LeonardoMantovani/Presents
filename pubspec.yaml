# This file is part of Presents.
# Presents is free software developed by LezSoft. While LezSoft holds all rights on the Presents brand and logo,
# you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
# Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
#
# SPDX-FileCopyrightText:  © 2020-2022 LezSoft <https://lezsoft.com>
# SPDX-License-Identifier: GPL-3.0-or-later

name: presents
description: The Ultimate Wishlist App

# The following line prevents the package from being accidentally published to
# pub.dev using `flutter pub publish`. This is preferred for private packages.
publish_to: 'none' # Remove this line if you wish to publish to pub.dev

version: 2.1.0+17

environment:
  sdk: '>=2.18.0 <3.0.0'

dependencies:
  flutter:
    sdk: flutter
  appwrite: ^8.1.0
  go_router: ^5.2.4
  flutter_web_plugins:
    sdk: flutter
  provider: ^6.0.4
  flutter_spinkit: ^5.1.0
  uuid: ^3.0.7
  url_launcher: ^6.1.7
  date_field: ^3.0.2
  photo_view: ^0.14.0
  image_picker: ^0.8.6
  intl: ^0.17.0
  shared_preferences: ^2.0.15
  receive_sharing_intent: ^1.4.5
  share_plus: ^6.3.0

dev_dependencies:
  flutter_lints: ^2.0.1
  flutter_launcher_icons: ^0.11.0

flutter_icons:
  android: true
  image_path_android: "assets/icons/complete.png"
  adaptive_icon_background: "assets/icons/background.png"
  adaptive_icon_foreground: "assets/icons/foreground.png"
  web:
    generate: true
    image_path: "assets/icons/complete.png"
    background_color: "#FFAB40FF"
    theme_color: "#FFAB40FF"

flutter:
  uses-material-design: true
  assets:
    - assets/images/
  fonts:
    - family: Poppins
      fonts:
        - asset: assets/fonts/Poppins/Poppins-Black.ttf
          weight: 900
        - asset: assets/fonts/Poppins/Poppins-BlackItalic.ttf
          weight: 900
          style: italic
        - asset: assets/fonts/Poppins/Poppins-ExtraBold.ttf
          weight: 800
        - asset: assets/fonts/Poppins/Poppins-ExtraBoldItalic.ttf
          weight: 800
          style: italic
        - asset: assets/fonts/Poppins/Poppins-Bold.ttf
          weight: 700
        - asset: assets/fonts/Poppins/Poppins-BoldItalic.ttf
          weight: 700
          style: italic
        - asset: assets/fonts/Poppins/Poppins-SemiBold.ttf
          weight: 600
        - asset: assets/fonts/Poppins/Poppins-SemiBoldItalic.ttf
          weight: 600
          style: italic
        - asset: assets/fonts/Poppins/Poppins-Medium.ttf
          weight: 500
        - asset: assets/fonts/Poppins/Poppins-MediumItalic.ttf
          weight: 500
          style: italic
        - asset: assets/fonts/Poppins/Poppins-Regular.ttf
        - asset: assets/fonts/Poppins/Poppins-Italic.ttf
          style: italic
        - asset: assets/fonts/Poppins/Poppins-Light.ttf
          weight: 300
        - asset: assets/fonts/Poppins/Poppins-LightItalic.ttf
          weight: 300
          style: italic
        - asset: assets/fonts/Poppins/Poppins-ExtraLight.ttf
          weight: 200
        - asset: assets/fonts/Poppins/Poppins-ExtraLightItalic.ttf
          weight: 200
          style: italic
        - asset: assets/fonts/Poppins/Poppins-Thin.ttf
          weight: 100
        - asset: assets/fonts/Poppins/Poppins-ThinItalic.ttf
          weight: 100
          style: italic
